@extends('layouts.master')

@section('title')
Post Form
@endsection

@section('content')

<h1> Post Creation </h1>
<form class="form-horizontal" role="form" method="POST" action="{{ url('post') }}" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="row">
        <div class="file-field input-field">
            <div class="btn">
                <span>Image</span>
                <input type="file" accept=".JPG, .JPEG, .PNG" name="image">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text" >
            </div>
            @if ($errors->has('image'))
                <span class="help-block">
                    <strong>{{ $errors->first('image') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    <div class="row">
        <div class="row">
            <div class="input-field col s12">
                <textarea id="textarea1" class="materialize-textarea" name="content" class="validate"></textarea>
                <label for="textarea1">Content</label>
            </div>
        </div>
            @if ($errors->has('content'))
                <span class="help-block">
                    <strong>{{ $errors->first('content') }}</strong>
                </span>
            @endif
    </div>
    <div class="row center">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="waves-effect waves-light btn">
                Create
            </button>
        </div>
    </div>
</form>
@endsection