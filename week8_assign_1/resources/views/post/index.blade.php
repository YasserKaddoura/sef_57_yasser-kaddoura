@extends('layouts.master')


@section('title')
Your Feed
@endsection

@section('content')
@foreach ($posts as $post)      
    <div class="row center">
        <div class="col s12">
    
            <div class="card">
      
                <div class="card-image" >
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="col s5">
                            <a href="{{ url('user/' . $post->user_id) }}" class="btn-floating btn-large waves-effect waves-light">
                                {{ HTML::image('img/profile/original/'.($post->user_id) . '.' . $post->user->ext, 'alt', array( 'width' => 60, 'height' => 60 )) }}
                            </a>
                            <p> {{ $post->user->user_name }} </p>  
                        </div>
                        <div class="col s6 center">
          
                            <p> {{ $post->created_at->format('M d') }} </p>
                        </div>
                    </div>
                    {{ HTML::image('img/post/original/' . $post->id . '.' . $post->user->ext) }}
                    <!-- Like Button -->
                    @if($post->liked != 0)

                        <form method="POST" action="{{ url('like/' . $post->liked) }}" id="unlike-form">
                            {{ csrf_field() }}
                            <input name="_method" type="hidden" value= 'DELETE' />
                        </form>
                        <a class="btn-floating halfway-fab waves-effect waves-light red btn-large"
                        onclick="event.preventDefault(); document.getElementById('unlike-form').submit();">
                            <i class="material-icons">thumb_down</i>
                            </a>

                    @else

                        <form method="POST" action="{{ url('like') }}" id="like-form">
                            {{ csrf_field() }}
                            <input name="post_id" type="hidden" value= {{ $post->id }} />
                        </form>
                        <a class="btn-floating halfway-fab waves-effect waves-light red btn-large"
                        onclick="event.preventDefault(); document.getElementById('like-form').submit();"><i class="material-icons">thumb_up</i></a>
                    
                    @endif
                    <!-- End like Button -->

                    <!-- Number of Likes -->
                    <a class="btn-floating halfway-fab waves-effect waves-light red center left red btn-large"> {{ $post->numLikes }} </a>
                </div>
                <div class="card-content">
                    <p>{{ $post->content }}</p>
                </div>
                <!-- COMMENTS -->
                @if (!empty($post->comments))
                    @foreach ($post->comments as $comment)
                        <div class="card-action">
                            <div class="row" style="margin-bottom: 0px;">
                                <div class="col s2">
                                    <a href="{{ url('user/' . $comment->user_id) }}" class="btn-floating btn-large waves-effect waves-light">
                                        {{ HTML::image('img/profile/original/' . $comment->user_id . '.' . $comment->user->ext, 'alt', array( 'width' => 60, 'height' => 60 )) }}
                                    </a>
                                    <p> {{ $comment->user->user_name }} </p>
                                </div>
                                <div class="col s8">
                                    <div class="row">
                                    </div>
                                    <div class="row">
                                        <p>{{ $comment->content }}</p>
                                    </div>
                                </div>
                                <div class="col s2 right">
                
                                    <p> {{ $comment->created_at->format('M d') }} </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <!-- COMMENTS END -->
                <!-- ADD COMMENT -->                
                <form class="form-horizontal" role="form" method="POST" action="{{ url('comment') }}">
                    {{ csrf_field() }}

                    <div class="card-action">
                        <div class="row" style="margin-bottom: 0px;">
                            <div class="col s2 left">
                                <a href="{{ url('user/' . Auth::id()) }}" class="btn-floating btn-large waves-effect waves-light">
                                {{ HTML::image('img/profile/original/' . Auth::id() . '.' . Auth::user()->ext, 'alt', array( 'width' => 60, 'height' => 60 )) }}</a>
                            </div>
                            <div class="col s8">
                                <div class="row">
                                </div>
                                <div class="row">
                                    <input type="text" id="textarea1" class="materialize-textarea" name="content" class="validate"/>
                                    @if ($errors->has('content'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('content') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                
                            </div>
                            <div class="col s2" style="margin-top:3%;">

                                <input name="post_id" type="hidden" value= {{ $post->id }} />
                                <button type="submit" class="waves-effect waves-light btn">
                                    Add
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>      
@endforeach

                
           
@endsection