@extends('layouts.master')

@section('title')

@endsection

@section('content')
<div class="row">
    <div class="col s12">
    </div>
</div>
<div class="row">
        {{ HTML::image('img/profile/original/' . $user->id . '.' . $user->ext, 'alt', array( 'width' => 300, 'height' => 300 )) }}
    <h5>{{ $user->user_name }}</h5>
    <h5>{{ $user->first_name . " " . $user->last_name}}</h5>
    <hr>
    <h5>posts : {{ count($posts->get()) }} </h5>
    <h5>followers : {{count($user->followers()->get()) }}</h5>
    <h5>following : {{count($user->following()->get()) }}</h5>
    <h5>likes : {{count($user->likes()->get()) }} </h5>
    <h5>comments : {{count($user->comment()->get()) }}</h5> 
</div>

@endsection