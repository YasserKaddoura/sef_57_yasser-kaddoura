@extends('layouts.master')

@section('title')
Users
@endsection

@section('content')

     
     
@foreach ($users as $user)
      <div class="col s12 m7">

    <div class="card horizontal">
        <div class="card-image">
            <a href="{{ url('user/' . $user->id) }}">
                {{ HTML::image('img/profile/original/' . $user->id . '.' . $user->ext, 'alt') }}
            </a>
        </div>
        <div class="card-stacked">
            <a href="{{ url('user/' . $user->id) }}">
                <span class="black-text name">
                    {{$user->user_name}} 
                </span>
            </a>
            <div class="card-content">
                <p>{{ $user->bio }}</p>
            </div>
            <div class="card-action">
                @if (Auth::id() != $user->id)
                    @if ($user->followed == 0)
                         <form class="form-horizontal" role="form" method="POST" action="{{ url('follower') }}">
                            {{ csrf_field() }}
                            <input name="user_id" type="hidden" value= {{ $user->id }} />
                            <button type="submit" class="waves-effect waves-light btn">
                                    Follow
                            </button>
                        </form>
                    @else 
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('follower/' . $user->id) }}">
                            {{ csrf_field() }}
                            <input name="_method" type="hidden" value= 'DELETE' />

                            <button type="submit" class="waves-effect waves-light btn">
                                    Unfollow
                            </button>
                        </form>
                    @endif
                @endif

            </div>
        </div>
    </div> 

@endforeach

@endsection