<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<body>
<!-- Top Navigator -->
  <nav class="blue accent-2" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">{{ config('app.name') }}</a>
      <!-- If logged in -->
      @if (!Auth::guest())
      <ul class="right hide-on-med-and-down">
        <li><a href="{{ url('post/create') }}">Add Post</a></li>
        <li><a href="{{ url('post') }}">Feed</a></li>
        <li><a href="{{ url('user')}}"> All Users </a></li>                
        <li><a href="{{ url('user/' . Auth::id()) }}">Profile</a></li>
        <li><a class="waves-effect" href= {{ route('logout') }} onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">Logout</a></li>
         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form></li>
      </ul>
      <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
      </div>
      @endif
    </nav>
    <!-- End Top Navigator -->
    @if (!Auth::guest())
    <!-- Side Navigator -->
    <ul id="slide-out" class="side-nav">
      <li><div class="userView">
        <div class="background">
          {{ HTML::image('img/background/images.jpeg') }}
        </div>
        <a href="{{ url('user/' . Auth::id()) }}">{{ HTML::image('img/profile/thumbnail/' . (Auth::id()) . '.' . Auth::user()->ext) }}</a>
        <a href="{{ url('user/' . Auth::id()) }}"><span class="white-text name"> {{ Auth::user()->user_name }}</span></a>
        <a href="#!email"><span class="white-text email"> {{ Auth::user()->email }}</span></a>
        </div>
      </li>
        <li><a href="{{ url('post') }}"><i class="material-icons">cloud</i>Feed</a></li>
        <li><a href="{{ url('post/create') }}">Add</a></li>
        
        <li><a href="{{ url('user/' . Auth::id()) }}"><i class="material-icons">perm_identity</i>Profile</a></li>
        <li><a href="{{ url('user')}}"> All Users </a></li>        
        <li><div class="divider"></div></li>
        <!-- Logout -->
        <li><a class="waves-effect" href= {{ route('logout') }} onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">Logout</a></li>
         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </ul>
    <!-- End Side Navigator -->
    @endif
       

    <div class="container">
    @yield('content')
    </div>

    <!-- Scripts -->
    <script>
     // Initialize collapse button
  $(".button-collapse").sideNav();
  // Initialize collapsible (uncomment the line below if you use the dropdown variation)
  //$('.collapsible').collapsible();
  </script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/materialize.js') }}"></script>
    <script src="{{ asset('js/init.js') }}"></script>
</body>
</html>
