@extends('layouts.master')


@section('title')
Login
@endsection

@section('content')

	<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
		{{ csrf_field() }}
		<div class="row">
			<div class="input-field col s12">
				<input id="email" type="text" class="validate" name="email">
				<label for="email">Email</label>
			</div>
			@if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
			@endif
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="password" type="password" class="validate" name="password">
				<label for="password">Password</label>
			</div>
			@if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
		</div>
		<div class="row center">
			<button type="submit" class="waves-effect waves-light btn">
				Login
			</button>
		</div>
	</form>
	<div class="row center">
		<a class="waves-effect waves-light btn" href= {{ route('register') }}>Not Registered Yet?</a>
	</div>
</div>
  
@endsection
