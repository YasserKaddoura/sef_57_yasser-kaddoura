<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            // The primary key for the table
            $table->increments('id');
            // The id of the user who commented
            $table->integer('user_id')->unsigned();
            // The id of the post where the comment was made on
            $table->integer('post_id')->unsigned();
            // The content of the comment
            $table->text('content');            
            $table->timestamps();
            
            // Indexing
            // Foreign key for the users table
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            // Foreign key for the posts table
            $table->foreign('post_id')
                  ->references('id')->on('posts')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
