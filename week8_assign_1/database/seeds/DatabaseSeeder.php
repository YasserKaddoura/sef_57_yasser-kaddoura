<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(App\User::class, 2)->create();
        $follower = factory(App\Follower::class, 2)->create();
        $post = factory(App\Post::class, 4)->create();
        $comment = factory(App\Comment::class, 4)->create();
        $like = factory(App\Like::class, 4)->create();

    }
}
