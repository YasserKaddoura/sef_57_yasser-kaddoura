<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        // Random details using faker clsss
        'user_name' => $faker->name,
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        // Random token using random generator 
        'remember_token' => str_random(10),
        'ext' => 'png',
        'bio' => $faker->sentence
                
    ];
});