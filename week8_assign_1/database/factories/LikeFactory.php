<?php

use App\Post;
use App\User;


$factory->define(App\Like::class, function (Faker\Generator $faker) {

    return [
        // Random user and post id from the tables
        'user_id' => rand(1, User::get()->count()),
        'post_id' => rand(1, Post::get()->count()),
    ];
});