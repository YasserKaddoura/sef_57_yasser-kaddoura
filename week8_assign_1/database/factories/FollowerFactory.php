<?php

use App\Follower;
use App\User;

$factory->define(App\Follower::class, function (Faker\Generator $faker) {

    return [
        // Random ids from the uesr table
        'followed_id' => rand(1, User::get()->count()),
        'follower_id' => rand(1, User::get()->count()),
    ];
});