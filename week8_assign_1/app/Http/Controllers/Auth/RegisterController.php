<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Storage;
use File;
use Intervention\Image\Facades\Image as Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/post';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'user_name' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {   
        $fileName = 'null';
        // Image extension
        $file_ext = 'null';

        // If user didn't include an image use a default one
        if (isset($data['image'])) {
            // Get the image
            $file = $data['image'];
            // Get the extension
            $file_ext = $file->getClientOriginalExtension();
        
        } else {
            // Set a default image
            $file = File::get(public_path('img/profile/default.png'));
            $file_ext = 'png';
            
        }

        // Create a user instance 
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'user_name' => $data['user_name'],
            'bio' => '',
            'ext' => $file_ext,
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);
        

        // The image name 
        $file_name = $user->id .'.' . $file_ext;

        // The paths for the image 
        $thumbnail_path = public_path('img/profile/thumbnail/');
        $original_path = public_path('img/profile/original/');

        // Resizing the image and storing it
        Image::make($file)
        ->resize(261,null,function ($constraint) {
        $constraint->aspectRatio();
            })
        ->save($original_path . $file_name)
        ->resize(90, 90)
        ->save($thumbnail_path . $file_name);

        return $user;
    }
}
