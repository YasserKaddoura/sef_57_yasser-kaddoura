<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class FollowerController extends Controller
{
 
    /**
     * Store the id of the user that the logged in user
     * wants to follow
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Store the user that the user is following
        Auth::user()->following()->save(User::find(request('user_id')));
        return redirect('/user');
    }

  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find the logged in user
        $user = Auth::user();
        // Remove the user that the logged in user is following 
        $user->following()->detach($id);
        return redirect('/user');
    }
}
