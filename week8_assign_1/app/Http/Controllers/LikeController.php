<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Like;
use Illuminate\Support\Facades\Auth;


class LikeController extends Controller
{

    /**
     * Store a newly created Like in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Instantiating a like
        $like = Like::create([
            'post_id' => (int)$request->post_id,
            'user_id' => Auth::id()
        ]);

        $like->save();
        // redirect
        return redirect('/post');
    }


    /**
     * Remove the like from a post that the user made
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get the like and remove it
        $like = Like::find($id);
        $like->delete();

        return redirect('/post');
        
    }
}
