<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Post;
use App\Like;
use App\Comment;
use File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


use Intervention\Image\Facades\Image as Image;

class PostController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display the feed
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // The logged in user id
        $id = Auth::id();
        // Get the posts of the user and the followers ordering it from
        // most rescent
        $posts = Post::whereIn('user_id', function($query) use($id)
        {
        $query->select('followed_id')
                ->from('followers')
                ->where('follower_id', $id);
        })->orWhere('user_id', $id)->latest()->get();

        foreach ($posts as $post) {
            // Like for each post
            $query= Like::where('post_id', $post->id);
            $post->numLikes = $query->count();
            // Did the user like the post
            $post->liked = $query->where('user_id', Auth::id())->first();
            
            // Get all the comments of each post
            $post->comments = Comment::where('post_id', $post->id)->get();
            // Check if the like exists
            if (!empty($post->liked)) {
                $post->liked = $post->liked->id;
            } else {
                $post->liked = 0;
            }
        }
       
        // load the view and pass the posts
        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the user's input
        $this->validate($request, [
            'image' => 'required',
            'content' => 'max:255'
        ]);
        
        // Instantiating a post
        $post = Post::create([
            'content' => request('content'),
            'user_id' => Auth::id(),
            'ext' => request('image')->getClientOriginalExtension()
        ]);
        $post->save();
        // Store the image 
        $this->storeImage(request('image'), $post->id);
        // redirect
        return redirect('/post');
    }
    /**
     * Store an image of a post
     *
     * @param  Image - the image of the post
     */
    private function storeImage($image, $postId) {
        // Get the image else there's an error
        if (isset($image)) {
            $file = $image;
            $file_name = $postId .'.' . $file->getClientOriginalExtension();
        } else {
            abort(400, 'Bad Request.');
        }

        // The paths for the image 
        $thumbnail_path = public_path('img/post/thumbnail/');
        $original_path = public_path('img/post/original/');

        // Resizing the image and storing it
        Image::make($file)
        ->resize(261,null,function ($constraint) {
        $constraint->aspectRatio();
            })
        ->save($original_path . $file_name)
        ->resize(90, 90)
        ->save($thumbnail_path . $file_name);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
