<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Illuminate\Support\Facades\Auth;


class CommentController extends Controller
{

    /**
     * Store a comment in the database
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Instantiate a new comment
        $comment = Comment::create([
            'post_id' => (int)$request->post_id,
            'user_id' => Auth::id(),
            'content' => request('content')
        ]);
        // Save it in the database
        $comment->save();
        // redirect
        return redirect('/post');
    }

    /**
     * Remove A comment
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $like = Like::find($id);
        $like->delete();

        return redirect('/post');
    }
}
