<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image', 'content', 'user_id', 'ext'
    ];

    /**
      * One - Many relationship with user
      *
      * Returns the user who made like
      */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
      * One - Many relationship with post
      *
      * Returns the lieks that's made on this post
      */
    public function like()
    {
        return $this->hasMany(Like::class);
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }
    
}
