<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'user_name',
         'email', 'password', 'bio', 'ext'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
      * One - Many relationship with posts
      *
      * Returns the posts made by the user
      */
    public function post()
    {
        return $this->hasMany(Post::class);
    }

    /**
      * One - Many relationship with comments
      *
      * Returns the comments made by the user
      */
    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    /**
      * One - Many relationship with likes
      *
      * Returns the likes made by the uesr
      */
    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    /**
      * Returns the followers for the user
      */
    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'followed_id', 'follower_id')->withTimestamps();
    }

    /**
      * Returns the following users for the user
      */
    public function following()
    {
        return $this->belongsToMany(User::class, 'followers', 'follower_id', 'followed_id')->withTimestamps();
    }
}
