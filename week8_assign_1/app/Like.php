<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    
    
    protected $fillable = [
        'post_id', 'user_id'
    ];

    /**
      * One - Many relationship with post
      *
      * Returns the post where the like was made on
      */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
      * One - Many relationship with user
      *
      * Returns the user who made like
      */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
