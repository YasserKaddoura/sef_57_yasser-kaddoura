<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    
     protected $fillable = [
        'post_id', 'user_id', 'content'
    ];

    /**
      * One - Many relationship with post
      *
      * Returns the post where the comment was made in
      */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
      * One - Many relationship with user
      *
      * Returns the user who made comment
      */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}
