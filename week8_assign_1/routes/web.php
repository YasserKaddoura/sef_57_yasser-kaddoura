<?php



// For authentication
Auth::routes();

Route::resource('post', 'PostController');
Route::resource('user', 'UserController');
Route::resource('like', 'LikeController');
Route::resource('follower', 'FollowerController');
Route::resource('comment', 'CommentController');
Route::get('/', function () {
    return redirect('post');
});