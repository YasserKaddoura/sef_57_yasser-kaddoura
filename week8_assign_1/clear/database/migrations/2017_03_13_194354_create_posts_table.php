<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            // The primaru key of the post
            $table->increments('id');
            // The user id who created the post
            $table->integer('user_id')->unsigned();
            // The content of the post if any
            $table->text('content');            
            // Tracking the time of changes and creation
            $table->timestamps();

            // Indexing
            // Foreign key for the user table
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
