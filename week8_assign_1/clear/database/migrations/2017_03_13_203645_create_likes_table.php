<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            // The id of the user who liked
            $table->integer('user_id')->unsigned();
            // The id of the post that was liked
            $table->integer('post_id')->unsigned();
            $table->timestamps();

            //Indexing
            // Foreign key for the user table
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            // Foreign key for the post table                  
            $table->foreign('post_id')
                  ->references('id')->on('posts')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
