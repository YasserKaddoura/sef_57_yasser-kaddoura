<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followers', function (Blueprint $table) {
            // The id of the user who is being followed
            $table->integer('user_id')->unsigned();
            // The id of the user who is following
            $table->integer('follower_id')->unsigned();    
            // Tracking the time of changes and creation
            $table->timestamps();
            
            // Indexing
            // Composite primary key on both attributes
            $table->primary(['user_id', 'follower_id']);
            // Both attributes are foreign keys for the users table
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            $table->foreign('follower_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('followers');
    }
}
