<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            // The primary key for the table
            $table->increments('id');
            // User's name
            $table->string('first_name');
            $table->string('last_name');
            $table->string('user_name')->unique();           
            // User's biography
            $table->text('bio');
            // User's email
            $table->string('email')->unique();
            $table->string('password');
            // Keep track of the session
            $table->rememberToken();
            // Tracking the time of changes and creation
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
