<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        // Random details using faker clsss
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        // Random token using random generator 
        'remember_token' => str_random(10),
    ];
});