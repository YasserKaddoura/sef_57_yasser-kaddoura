<?php

use App\Post;
use App\User;


$factory->define(App\Comment::class, function (Faker\Generator $faker) {

    return [
        // Random user and post id from the tables
        'user_id' => rand(1, User::get()->count()),
        'post_id' => rand(1, Post::get()->count()),
        // Random sentences for the content
        'content' => implode(', ', $faker->sentences), 
        'remember_token' => str_random(10),
    ];
});