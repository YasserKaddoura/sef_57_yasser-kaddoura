<?php

use App\User;

$factory->define(App\Post::class, function (Faker\Generator $faker) {
       
    return [
        // Random id from the user table
        'user_id' => rand(1, User::get()->count()),
        // Random sentences as content
        'content' => implode('\n', $faker->paragraphs)
    ];
});
