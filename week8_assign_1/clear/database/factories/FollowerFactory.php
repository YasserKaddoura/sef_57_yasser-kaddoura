<?php

use App\User;

$factory->define(App\User::class, function (Faker\Generator $faker) {

    return [
        // Random ids from the uesr table
        'user_id' => rand(1, User::get()->count()),
        'follower_id' => rand(1, User::get()->count()),
        'remember_token' => str_random(10),
    ];
});