@extends('layouts.master')


@section('title')
Registration
@endsection


@section('content')

<div class="container">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
            
        <div class="row">
            <div class="input-field col s6">
                <input id="first_name" type="text" class="validate" name="first_name" value="first">
                <label for="first_name">First Name</label>
                @if ($errors->has('first_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
                @endif
            </div>
            <div class="input-field col s6">
                <input id="last_name" type="text" class="validate" name="last_name" value="second">
                <label for="last_name">Last Name</label>
                @if ($errors->has('last_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="username" type="text" class="validate" name="user_name" value="username">
                    <label for="username">Username</label>
                </div>
                @if ($errors->has('user_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_name') }}</strong>
                </span>
                @endif
            </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="password" type="password" class="validate" name="password" value="123456">
                <label for="password">Password</label>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="password-confirm" type="password" class="validate" name="password_confirmation" value="123456">
                <label for="password-confirm">Confirm Password</label>
                @if ($errors->has('password-confirm'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password-confirm') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="email" type="email" class="validate" name="email" value="first@second.com">
                <label for="email">Email</label>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>Profile Picture</span>
                <input type="file" accept=".JPG, .JPEG, .PNG" name="image">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
            </div>
            
        </div>
        <div class="row center">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="waves-effect waves-light btn">
                    Register
                </button>
            </div>
        </div>
    </form>
    
</div>              
@endsection
