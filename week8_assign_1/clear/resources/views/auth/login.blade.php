@extends('layouts.master')


@section('title')
Login
@endsection

@section('content')
<div class="container">
	<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
		{{ csrf_field() }}
		<div class="row">
			<div class="input-field col s12">
				<input id="email" type="text" class="validate">
				<label for="email">Email</label>
			</div>
		</div>
		<div class="row">
			<div class="input-field col s12">
				<input id="password" type="password" class="validate">
				<label for="password">Password</label>
			</div>
		</div>
		<div class="row center">
			<button type="submit" class="waves-effect waves-light btn">
				Login
			</button>
		</div>
	</form>
</div>
  
@endsection
