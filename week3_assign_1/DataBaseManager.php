<?php
//php doesn't support enums so classes are being used
class Action 
{
    const CREATE = 0;
    const READ = 1;
    const UPDATE = 2;
    const DELETE = 3;
}
class Type 
{
    const DB = 0;
    const RELATION = 1;
    const TUPLE = 2;
}

class DataBaseManager 
{
    //custom user info
    const USERNAME = "admin";
    const PASSWORD = "admin";

    //info about the query
    private $action;
    private $type;

    //not enough suffecient info
    private $default;

    //status
    private $error;
    private $success;

   function __construct($username, $password) 
   {
        $this->error = "";
        $this->success = "";
        $this->authenticate($username, $password);
    }
    /*function __construct() {
        $this->error = "";
        $this->success = "";
    }
*/
    //user authentication
    private function authenticate($username, $password) 
    {
        if ($username != self::USERNAME || $password != self::PASSWORD) {
            $this->error = "Unauthorized user detected!";
        }
    }

    function query($rawCommand) 
    {
        //checks the query action and type + returns the arguments
        $arguments = $this->parser($rawCommand);
        if ($this->error != ''){
            return;
        }
        //executes the proper function depending on the type and action
        if ($this->type == Type::DB && $this->action == Action::CREATE) {
            $this->createDatabase($arguments);
        }elseif ($this->type == Type::DB && $this->action == Action::DELETE) {
            $this->deleteDatabase($arguments);            
        }elseif ($this->type == Type::RELATION && $this->action == Action::CREATE) {
            $this->createTable($arguments);
        }elseif ($this->type == Type::RELATION && $this->action == Action::DELETE) {
            $this->deleteTable($arguments);            
        }elseif ($this->type == Type::TUPLE && $this->action == Action::CREATE) {
            $this->addRecord($arguments);
        }elseif ($this->type == Type::TUPLE && $this->action == Action::DELETE) {
            $this->deleteRecord($arguments);            
        }elseif ($this->type == Type::TUPLE && $this->action == Action::READ) {
            $this->getRecord($arguments);
        }
    }
    //checks the existance of the id in a table
    private function idChecker($records, $searchKey)
    {
        foreach ($records as $index => $record) {
            if ($record["ID"] == $searchKey) {               
                return $index;
            }
        }
        return FALSE;
    }
    
    //searches for a record depending on a key
    private function getRecord($args)
    {
        if (count($args) != 1) {
            $this->error = 'Wrong number of arguments!';
            return;
        }

        //takes the tableinfo from metadata
        $tables = $this->getMetaData('Tables');
        if ($tables == NULL) {
            $this->error = "Not a single table exists";
            return;
        }
        end($tables);
        $tableName = key($tables);
        $tableAts = $tables[$tableName];
        //check if the key is an int
        $searchKey = $args[0];
        if ((string)(int)$searchKey != $searchKey) {
            $this->error = 'The ID should be an int';
            return;
        }
         $database = $this->getMetaData('LastDataBaseCreated');
        
      
        $availRawMetaData = file_get_contents($database . '.db/' . $tableName . '.tb');
        $records = json_decode($availRawMetaData, true);

        if ($availRawMetaData == NULL) {
            $this->error = 'The table is empty!';
            return;
        }
        //iterate through the records
        $recordCheck = $this->idChecker($records, $searchKey);
        if ($recordCheck !== FALSE) {
            $atts= $records[$recordCheck];
            $lastAtt = array_pop($atts);
            foreach ($atts as $att) {
                printf('"%s",', $att);
            }
            printf("\"%s\"\n", $lastAtt);
            return;
        }
        $this->error = "The record doesn't exist";
    }
    private function defaultMode() 
    {
        $defaultMode = array ();

        $tables = $this->getMetaData('Tables');
        $dataBaseName = $this->getMetaData('LastDataBaseCreated');
        if ($tables == NULL) {
            $this->error = 'Not a single table exists';
            return;
        }
        end($tables);
        $tableName = key($tables);
        $tablePath = $dataBaseName . '.db/' . $tableName . '.tb';
        $tableAts = $tables[$tableName];

        array_push($defaultMode, $tablePath, $tableAts);
        
        return $defaultMode;
    } 

    private function deleteRecord($args) 
    {
        array_shift($args);
        if (count($args) != 1) {
            $this->error = 'Wrong number of arguments!';
            return;
        }
        $searchKey = $args[0];
        //checks if the id is an int
        if ((string)(int)$searchKey != $searchKey) {
            $this->error = 'The ID should be an int';
            return;
        }
        $defaultMode = $this->defaultMode();
      //  $tables = $this->getMetaData('Tables');
       // end($tables);
        if ($this->error != '') {
            return;
        }
        $tableAts = array_pop($defaultMode);    
        $tableName = array_pop($defaultMode);        
        $availRawMetaData = file_get_contents($tableName);
        $records = json_decode($availRawMetaData, true);

        if ($availRawMetaData == NULL) {
            $this->error = 'The table is empty!';
            return;
        }
        //to check if the record got deleted
        $recordDeleted = FALSE;
        $recordCheck = $this->idChecker($records, $searchKey);
        if ($recordCheck !== FALSE) {
            unset($records[$recordCheck]);
                $recordDeleted = TRUE;
            }
        if (!$recordDeleted) {
            $this->error = "The Record doesn't exist.";
            return;
        }        
        file_put_contents($tableName, json_encode($records, JSON_PRETTY_PRINT));        
        $this->success = 'Record has been deleted';        
    }

    private function addRecord($args)
    {
        if ( count($args) < 2) {
            $this->error = 'Wrong number of arguments!';
            return;
        }
        //get table info
        $tables = $this->getMetaData('Tables');
        if ($tables == NULL) {
            $this->error = "Not a single table exist.";
            return;
        }
        //get the last table created
        end($tables);
        $tableName = key($tables);
        $tableAts = $tables[$tableName];
       
        //valid number of elements for the record?
        if (count($tableAts) != count($args)) {
            $this->error = 'Wrong number of columns!';
            return;
        }
        //valid ID
        $id = (string)(int)$args[0];
        if ( $id != $args[0] || $id <= 0) {
            $this->error = 'Invalid ID!';
            return;
        }
        //check if the key exists already
        $database = $this->getMetaData('LastDataBaseCreated');       
      
        $availRawMetaData = file_get_contents($database . '.db/' . $tableName . '.tb');
        $records = json_decode($availRawMetaData, true);
        
        if ($records == NULL) {
            $records = array ();
        }
        //checks for id uniqueness
        $recordCheck = $this->idChecker($records, $id);
        if ($recordCheck !== FALSE) {
            $this->error = "The ID {$args[0]} already exists";
            return;
        }
      /*  if ($this->getRecord($args[0], TRUE)){
            $this->error = "The ID {$args[0]} already exists";
            return;
        }*/

        //populate the record
        $record = array ();
        for ($i = 0; $i < count($tableAts); $i++) {
            //echo $tableAt . "\n";
            $record[$tableAts[$i]] = $args[$i];            
        }
         
        array_push($records, $record);     
        file_put_contents($database . '.db/' . $tableName . '.tb', json_encode($records, JSON_PRETTY_PRINT));
        $this->success = 'Record has been created';        
    }

    private function deleteDatabase($args)
    {
        if (count($args) > 2) {
            $this->error = 'Remove the extra arguments!';
            return;
        }
        //if database doesn't exist'
        if (!is_dir($args[1].'.db')) {
            $this->error = "{$args[1]} database exist!";
            return;
        }
        //database removal
        array_map('unlink', glob("{$args[1]}.db/*.*"));
        rmdir($args[1].'.db');
        $this->success = 'Database has been deleted';
        $this->metaDataUpdater($args[1], 'DeleteDatabase');
    }

    private function createDatabase($args)
    {
        if (count($args) > 2) {
            $this->error = 'Remove the extra arguments!';
            return;
        }
        $databaseName = $args[1];
        //if database exists
        if (is_dir($databaseName.'.db')) {
            $this->error = "{$databaseName} Database already exist!";
            return;
        }   
        //database creation
        mkdir("./{$databaseName}.db");
        //update meta-data file
        $this->metaDataUpdater($databaseName, 'Databases');
        $this->metaDataUpdater($databaseName, 'LastDataBaseCreated');
        $this->success = "{$databaseName} Database has been created";            
    }    

    //argument validation
    private function isValidArgument($argument)
    {
        return substr($argument, -1) == '"' && substr($argument, 0, 1) == '"';
    }
    
    private function deleteTable($args)
    {
        if (count($args) > 2) {
            $this->error = 'Remove the extra arguments!';
            return;
        }
        $database = $this->getMetaData('LastDataBaseCreated');
 
        $tableName = $args[1];
        if (!file_exists($database . '.db/' . $tableName . '.tb')) {
            $this->error = "$tableName table doesn't exist!";
            return;
        }
        unlink($database . '.db/' . $tableName . '.tb');
        
        $tableInfo = array();
        array_push($tableInfo, $database, $tableName);
        $this->metaDataUpdater($tableInfo, 'DeleteTable');        
        $this->success = 'Table has been deleted';        
    }

    private function createTable($args)
    {
        array_shift($args);
        //validify the command
        if (count($args) < 3) {
            $this->error = "Wrong amount of arguments!";
            return;
        }
        if (strtolower($args[1]) != 'columns') {
            $this->error = "You need to have columns as the 4th argument!";
            return;
        }
        $database = $this->getMetaData('LastDataBaseCreated');
        //get the table name and check its existance
        $tableName = array_shift($args);
        if (file_exists($database . '.db/' . $tableName . '.tb')) {
            $this->error = "{$tableName} Table already exist!";
            return;
        }
        if ($database == NULL) {
            $this->error = 'No Database found.';
            return;
        }
        //creates the database
        $stream = fopen($database . '.db/' . $tableName . '.tb', 'w');
        fclose($stream);

        //readies the info for meta-data file     
        $tableInfo = array ();
        array_push($tableInfo, $database, $tableName, 'ID');
        //stores the tale attributes
        for ($i = 1; $i < count($args); $i++) {
            array_push($tableInfo, $args[$i]);
        }
        //update meta-data file
        $this->metaDataUpdater($tableInfo, 'Tables');
        $this->success = "{$tableName} table has been created";        
    }

 
    
  
    private function metaDataUpdater($metaDataInfo, $metaDataTarget)
    {
        //get meta-data content
        $availRawMetaData = file_get_contents('meta-data');         
        $availMetaData = json_decode($availRawMetaData, true);
        //update the chosen meta-data
        switch ($metaDataTarget) {
            case 'LastDataBaseCreated':
                $availMetaData['LastDataBaseCreated'] = $metaDataInfo;
                break;
            case 'Databases':
                $dataBaseName = $metaDataInfo;
                $availMetaData['Databases'][$dataBaseName] = NULL;
                break;
            case 'Tables':
                $dataBaseName = array_shift($metaDataInfo);
                $tableName  = array_shift($metaDataInfo);                
                $availMetaData['Databases'][$dataBaseName][$tableName] = $metaDataInfo;
                break;
            case 'DeleteTable':
                $dataBaseName = array_shift($metaDataInfo);
                $tableName = array_shift($metaDataInfo);
                unset($availMetaData['Databases'][$dataBaseName][$tableName]);
                break;
            case 'DeleteDatabase':
                unset($availMetaData['Databases'][$metaDataInfo]);                
        }
        //store  the new meta-data
        file_put_contents('meta-data', json_encode($availMetaData, JSON_PRETTY_PRINT));
    }    

   private function getMetaData($metaDataTarget)
   {
        if (!file_exists('meta-data')) {
            $stream = fopen('meta-data', 'w');
            fclose($stream);
        }
        $availRawMetaData = file_get_contents('meta-data');    
        $availMetaData = json_decode($availRawMetaData, true);
        switch ($metaDataTarget) {
            case 'LastDataBaseCreated':
                $databases = $availMetaData['Databases'];
                if ($databases == NULL){
                    return;
                }
                end($databases);
                return key($databases);
                break;
            case 'Databases':
                return $availMetaData['Databases'];
                break;
            case 'Tables':              
                $database = $this->getMetaData('LastDataBaseCreated');
                if ($database == NULL){
                    return;
                }
                return $availMetaData['Databases'][$database];
                break;
        }
   }

    private function parser($rawCommand)
    {
        $arguments = explode(",", $rawCommand);        
        if (count($arguments) < 2) {
            $this->error = "Invalid command";
            return;
        }
        $firstArg = strtolower(trim(array_shift($arguments)));
        $secondArg = strtolower(trim($arguments[0])); 
        
        //checks for valid command
        switch ($firstArg) {
            case "create":
                //echo Action::CREATE;
                $this->action = Action::CREATE;
                if ($secondArg == "database") {
                    $this->type = Type::DB;
                }elseif ($secondArg == "table") {
                    $this->type = Type::RELATION;
                }else {
                    $this->error = "Invalid create command";
                    return;
                }
                break;                
            case "add":
                $this->action = Action::CREATE;
                $this->type = Type::TUPLE;
                /*if ($this->isValidArgument($secondArg)) {
                    $this->default = TRUE;
                    }
                if ($secondArg == "row") {
                }else {
                    $this->error = "Invalid add command";
                    return;
                }*/
                break;              
            case "get":
                $this->action = Action::READ;
                $this->type = Type::TUPLE;
                
                /*if ($this->isValidArgument($secondArg)) {
                }if ($secondArg == "column" || $secondArg == "table") {
                    $this->type = Type::TUPLE;
                }else {
                    $this->error = "Invalid get command";
                    return;
                }*/
                break;
            case "delete":
                $this->action = Action::DELETE;
                if ($this->isValidArgument($secondArg)) {
                    $this->default = TRUE;
                }elseif ($secondArg == "row") {
                    $this->type = Type::TUPLE;
                }elseif ($secondArg == "table") {
                    $this->type = Type::RELATION;
                }elseif ($secondArg == "database") {
                    $this->type = Type::DB;                
                }else {
                    $this->error = "Invalid delete command";
                    return;
                }
                break;                
            default:
                $this->error = "Invalid commandsad";
                return;
                break;
        }
        return $arguments;      
    }

    function checkError()
    {
        if ($this->error != '') {
            $tempErr = $this->error;            
            $this->error = '';
            return $tempErr;
        }
    }   
    function checkSuccess()
    {
       if ($this->success != '') {
            $tempSucc = $this->success;            
            $this->success = '';
            return $tempSucc;
        } 
    }
}
?>
