#!/usr/bin/php
<?php
require_once 'DataBaseManager.php';

//validate the user
print("USERNAME:\n");
$username = readline('');
print("PASSWORD:\n");
$password = readline('');
$con = new DataBaseManager($username, $password);

$error = $con->checkError();
if ($error != '') {
    printf("An error has occured : %s " . PHP_EOL, $error);
    exit();
}

//$con = new DataBaseManager();


print "Database Mode ON:\n";
//get the user commands until he decides to exit
while(TRUE){
    $command = readline('>');
    if ($command == 'close') {
        exit("See you later.\n");
    }
    //tries to execute the query if no errors are present
    $con->query($command);
    $error = $con->checkError();
    if ($error != '') {
        printf("An error has occured : %s " . PHP_EOL, $error);
    }
    $success = $con->checkSuccess();    
    if($success != ''){
        printf("Success : %s " . PHP_EOL, $success);
    }
}
?>