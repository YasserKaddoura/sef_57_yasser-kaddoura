<?php



Route::get('/', function () {
    return redirect('/posts');
});
// Shows the list of posts
Route::get('posts', 'PostsController@index');
// Goes the post creation form
Route::get('posts/create', 'PostsController@create');
// Shows a specific post
Route::get('posts/{post}', 'PostsController@show');
// Shows an edit form for a specific post
Route::get('posts/{post}/edit', 'PostsController@showEdit');
Auth::routes();
// Removea a post in the database
Route::delete('posts/{id}', 'PostsController@destroy');
// Updats a post in the database
Route::put('posts/{id}', 'PostsController@update');
// Store a post in the database
Route::post('/posts', 'PostsController@store');


