<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * She the list of blogs
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retrieve the posts and order them descending
        $posts = Post::orderBy('created_at', 'desc')->paginate(3);
        // Store the user name and get a substring of the content
        foreach ($posts as $post) {
            $post->authorName = $post->user->name;
            // Get only the first 200 characters of the post
            if (strlen($post->content) > 200) {
            $post->content = substr($post->content, 0, 200) . '...';
            }
        }
        return view('posts', compact('posts'));
    }
    
    /**
     * Show a single post 
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {   
        // Get the author name and id with the post
        $post->authorName = $post->user->name;
        $post->authorId = $post->user->id;
        return view('posts.post', compact('post'));
    }

    /**
     * Direct to the create post page
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        if (Auth::guest()) {
            return redirect('login');
        }
        return view('posts.create');
    }

    /**
     * Insert a post in the database
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {   
        $post = new Post;

        $post->user_id = Auth::id();;
        $post->content = request('content');
        $post->title = request('title');
        $post->save();
        return redirect('/posts');
    }

    /**
     * Remove a post from the database
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $post = Post::find($id);
        $post->delete();
        return redirect('/posts');
        
    }
    /**
     * Show the edit page if authorized
     *
     * @return \Illuminate\Http\Response
     */
    public function showEdit(Post $post)
    {   
        if (Auth::id() != $post->user->id) {
            abort(403, 'Unauthorized action.');
        } else {
            $post->authorName = $post->user->name;
            $post->authorId = $post->user->id;
            return view('posts.edit', compact('post'));
        }
    }

    /**
     * Update a post in the database
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {   
        $post = Post::find($id);     
        if (Auth::id() != $post->user->id) {
            abort(403, 'Unauthorized action.');
        } else {
            $post->title = request('title');
            $post->content = request('content');
            $post->save();
        }
        // redirect
        return redirect('/posts');
    }
}
