@extends('layouts.app')

@section('content')

    <div class="container-narrow">


        <div class="row-fluid marketing">
            <div class="list-posts">
                <hr>
                @foreach ($posts as $post)        
                    <a class="post-all" href="/posts/{{ $post->id }}">
                        <h3 class='title-all'>{{ $post->title }}</h4>
                        <h6 class='author-name-all'>{{ $post->authorName }}</h6>
                        <h6 class='date-created-all'>{{ $post->created_at->format('M d') }}</h6>
                        <p class='post-content-all'>{{ $post->content }}</p>
                    </a>
                    <hr>
                @endforeach
            </div>
            {{ $posts->links() }}
        </div>
    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->

@endsection
