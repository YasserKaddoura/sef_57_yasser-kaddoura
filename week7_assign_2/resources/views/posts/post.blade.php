@extends('layouts.app')

@section('content')
<div class="container-narrow">

    <div class="row-fluid marketing">
        <div class="article">
            <h3 class='title'>{{ $post->title }}</h4>
            <h6 class='author-name'>created by {{ $post->authorName }}</h6>
            <h6 class='date-created'>at {{ $post->created_at->format('M d') }}</h6>
            <p class='post-content'>{{ $post->content }}</p>
            @if (Auth::id() == $post->authorId)
                <form method="POST" action="/posts/{{$post->id}}">
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="DELETE">
                    <input type="submit" class="btn btn-primary" value="Delete"/>
                </form>
                <form method="GET" action="{{$post->id}}/edit">
                    <input type="submit" class="btn btn-primary" value="Edit"/>
                </form>
            @endif
        </div>

    </div>

</div>
@endsection