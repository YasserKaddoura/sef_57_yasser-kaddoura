@extends('layouts.app')

@section('content')
<div class="container-narrow">

    <div class="row-fluid marketing">
       
        <h1>EDIT POST</h1>

        <hr>

        <form method="POST" action="/posts/{{$post->id}}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" placeholder="Title" value="{{$post->title}}" name="title" required>
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea onkeyup="textAreaAdjust(this)" style="overflow:hidden" type="text" class="form-control" id="content" placeholder="Content" name="content" required>{{$post->content}}
                </textarea>
            </div>
                <input name="_method" type="hidden" value="PUT">
                <input type="submit" class="btn btn-primary" value="Finish Editing"/>
        </form>
    </div>

</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script> 
<script>
    // Resized the text area when a character is inserted
    function textAreaAdjust(o) {
        o.style.height = "1px";
        o.style.height = (25 + o.scrollHeight)+"px";
}
</script>
@endsection