@extends('layouts.app')

@section('content')
<div class="container-narrow">

    <div class="row-fluid marketing">
    <h1>CREATE POST</h1>

    <hr>

    <form method="POST" action="/posts">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" placeholder="Title" name="title" required>
        </div>
        <div class="form-group">
            <label for="content">Content</label>
            <textarea type="text" class="form-control" id="content" placeholder="Content" name="content" required>
            </textarea>
        </div>

        <button type="submit" class="btn btn-primary">Publish</button>
    </form>
     </div>
    </div>
@endsection