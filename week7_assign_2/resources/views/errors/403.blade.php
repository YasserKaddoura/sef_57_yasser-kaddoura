@extends('layouts.app')


@section('content')
    <div class="container-narrow">
        <div class="error-page">
            <h1 class="status-code"> 403 </h1>
            <h3> Unauthorized User! </h3>
        </div>
    </div>
@endsection