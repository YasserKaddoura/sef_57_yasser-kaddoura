@extends('layouts.app')


@section('content')
    <div class="container-narrow">
        <div class="error-page">
            <h1 class="status-code"> 404 </h1>
            <h3> Page not found </h3>
        </div>
    </div>
@endsection