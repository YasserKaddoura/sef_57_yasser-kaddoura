#!/bin/bash

# SE-Factory Week1_Assign1_Ex2
# Author: Yasser Kaddoura
# Version: 0.2

# Checks the memory
function memory {
    MEM_USAGE=`free | grep Mem | awk '{print $3/$2 * 100.0}'`;

    # Check if the memory memory usage is bigger or equal than 80
    # bc is used since bash doesn't like floats
    MEM_DANGER=`echo "$MEM_USAGE>=80" | bc`;

    if [ $MEM_DANGER -eq "1" ]
        then
            echo "ALARM: Virtual Memory is at ${MEM_USAGE}%";
    fi
}

# Checks the space
function space {
    drives=$(df -H | grep -vE '^Filesystem|tmpfs|cdrom|udev')

    while read line; do  

        used_space=$(echo "$line" | awk '{ print $5 }' | tr -dc '0-9')    
        
        if [ "$used_space" -ge 80 ]
        then
            drive_name=`echo "$line" | awk '{ print $1 }'`
            echo "\nALARM: Disk ${drive_name} is at ${used_space}%"
        fi 

    done <<< "$drives"
}

# Outputs the result
function output {
    # If parameter is empty
    if [ -z "$1" ] 
        then 
            echo 'Everything is OKAY'; 
        else 
            echo -e $1; 
    fi
}

# Check if something wrong
STATUS="$(memory)$(space)"

output "$STATUS"
