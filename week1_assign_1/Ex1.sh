#!/bin/bash

# SE-Factory Week1_Assign1_Ex1
# Author: Yasser Kaddoura
# Version: 0.2

# Where's the csv will be created
DESTINATION=/home/$USER/Desktop;

# Get the list of files inside log directory
FILES_LIST=`ls -l /var/log`;

# Get the files that has only log extension
LOGS_LIST=`echo "$FILES_LIST" | grep ".*\\.log$"`;

# Extract the name and the size of the files
FORMATED_LIST=`echo "$LOGS_LIST" | awk '{print $9, $5/1024}'`;

# Add a comma between the fields
FINALSTATE_LIST=`echo "$FORMATED_LIST" | sed 's/\.log\b/&,/'`;

# Insert the result into the csv file
echo "$FINALSTATE_LIST" > $DESTINATION/log_dump.csv;