
<?php
    require_once "Stack.php";
    class GameSolver{
        const OPERATORS = array ("-", "+", "*", "/");

        //solution storage
        private $numStackSolution;
        private $operStackSolution;
        private $result = 0;

        private $target;
        private $emptyStack;

        function __construct($set, $target){
            rsort($set);
            $numRemain = $set;
            $this->target = $target;
            $this->emptyStack = new Stack();

            $this->numStackSolution = clone $this->emptyStack;
            $this->operStackSolution = clone $this->emptyStack;
            //start from the first/biggest element of the set
            $currNumber = array_shift ($numRemain);
            $numStack = clone $this->emptyStack;
            $numStack->push ($currNumber);
            $this->gameSolver ($currNumber, $numRemain, $numStack, clone $this->emptyStack);
            
            //just to cover a wider set of casess (REMOVE)
            $numRemain = array_reverse($set);
            $currNumber = array_shift($numRemain);

            $numStack = clone $this->emptyStack;
            $numStack->push($currNumber);
            $this->gameSolver($currNumber, $numRemain, $numStack, clone $this->emptyStack);
        }
   


        function gameSolver ($currNumber, $numRemain, $numStack , $operStack){          
            $size = count ($numRemain);
            
            if ($size == 0){
                return $currNumber;
            }
            $tempList = $numRemain;
            $Number = $currNumber;
            //not to lose the stacks while looping
            $numStacktemp = clone $numStack;
            $operStacktemp = clone $operStack;
            //var_dump($numStacktemp);
            
            for ($i = 0; $i < $size; $i++){
                //restoring the stacks
                $numStack = clone $numStacktemp;
                $operStack = clone $operStacktemp;

                $numRemain1 = $tempList;
                $number = $numRemain1[$i];
                unset ($numRemain1[$i]);
                $numRemain = array_values ($numRemain1);
                //reseting the list of operators
                $operators = self::OPERATORS;
                //starring with another case of operations from the next number
                $ttstack = clone $this->emptyStack;
                $ttstack->push ($number);
                $this->gameSolver ($number, $numRemain, $ttstack, clone $this->emptyStack);  
                $numStack->push ($number);
                //not to lose the stack progress while looping
                $operStack1 = clone $operStack;
                //loop through every operator
                for ($j = 0 ; $j < 4; $j++){
                    //refreshing the number and stack
                    $currNumber = $Number;
                    $operStack = clone $operStack1;  
                    //execute operator and store it in a stack       
                    $operator = array_shift ($operators);
                    $operStack->push ($operator);                 
                    echo" $currNumber{$operator}$number\n";
                    //check the opereation and start anew
                    switch ($operator){
                        case "+":
                            $currNumber += $number;
                            $this->Checker ($currNumber, $numStack, $operStack);                                                        
                            $this->gameSolver ($currNumber, $numRemain, $numStack, $operStack);   
                            echo "$currNumber";                             
                            //yield $currNumber;
                            
                            break;
                        case "-":
                            $currNumber -= $number;
                            if ($currNumber > 0){
                                $this->Checker($currNumber, $numStack, $operStack);                                    
                                $this->gameSolver ($currNumber, $numRemain, $numStack, $operStack);  
                            echo "$currNumber";                             
                                                              
                            //yield $currNumber;
                                                            
                            }
                            break;
                        case "*":
                            $currNumber *= $number;
                            $this->Checker($currNumber, $numStack, $operStack);
                            $this->gameSolver ($currNumber, $numRemain, $numStack, $operStack);      
                            echo "$currNumber";                             
                                                
                            //yield $currNumber;
                        
                            break;
                        case "/":
                            $currNumber /= $number;
                            if (is_int ($currNumber)){
                                $this->Checker($currNumber, $numStack, $operStack);                                
                                $this->gameSolver ($currNumber, $numRemain, $numStack, $operStack);   
                            echo "$currNumber";                             
                                                             
                            //yield $currNumber;
                                                                  
                            }
                            break;                            
                    }
                    
                    

                }
            }
        }
        //stores the result if it's nearer to the arget
        private function Checker($currNumber, $numStack, $operStack){
            if (abs($currNumber - $this->target) < abs($this->target - $this->result)){
                $this->result = $currNumber;
                $this->numStackSolution = $numStack;
                $this->operStackSolution = $operStack;
                //var_dump($this->numStackSolution);
                //var_dump($this->operStackSolution);
                //echo "NEAR:  ".$this->result."\n";
            }
        }
        function getNumStackSolution(){
            return $this->numStackSolution;
        }

        function getOperStackSolution(){
            return $this->operStackSolution;
        }

        function getResult(){
            return $this->result;
        }
    }

?>