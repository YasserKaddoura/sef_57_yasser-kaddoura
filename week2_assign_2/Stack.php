<?php
    class Stack{
        private $array;
        private $maxSize;
        private $top;

        function __construct($maxSize = 10){
            $this->array = array ();
            $this->top = -1;
            $this->maxSize = $maxSize;
        }

        function pop(){
            if ($this->isEmpty()){
                return;
            }
            $element = $this->array [$this->top--];
            return $element;
        }

        function push($element){
            if($this->top >= $this->maxSize){                
                return;
            }
            $this->array [++$this->top] = $element;
        }

        function peak(){
            return $this->array [$this->top];
        }

        function isEmpty(){
            return $this->top < 0 ? TRUE : FALSE;
        }

        function getSize(){
            return $this->top;
        }

        function flip(){
            $this->array = array_reverse($this->array);
        } 
    }

?>