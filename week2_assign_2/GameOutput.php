<?php
    Class GameOutput{
        private $gameSolution;

        function __construct($gameNumber, $gameSet, $gameTarget, $gameSolution){
            $this->gameSolution = $gameSolution;
            $this->solutionParser ($gameNumber, $gameSet, $gameTarget, $gameSolution);              
        }

        private function solutionParser($gameNumber, $gameSet, $gameTarget, $gameSolution){
            $gameSetSize = count ($gameSet);
            //to be parsed values
            $gameNumStackSolution = $gameSolution->getNumStackSolution();
            $gameOperStackSolution = $gameSolution->getOperStackSolution();
            $gameStatus = $gameTarget - $gameSolution->getResult();
            $totalNumUsed = $gameNumStackSolution->getSize();
            
            $gameNumStackSolution->flip();
            $gameOperStackSolution->flip();
            //var_dump($gameNumStackSolution);
            $output = "Game {$gameNumber}:\n{ ";

            //game set+target
            for ($i = 0; $i < $gameSetSize - 1; $i++){
                $output .= "{$gameSet[$i]}, ";
            }

            $firstNum = $gameNumStackSolution->pop();
            
            $output .= "{$gameSet[$gameSetSize - 1]} }\nTarget: {$gameTarget}\n\nSolution ";

            //game status
            if ($gameStatus == 0){
                $output .= "[Exact]:\n";
            }elseif ($gameStatus < 0){
                $output .= "[Remaining: {$gameStatus}]\n";
            }else {
                $output .= "[Remaining: +{$gameStatus}]\n";
            }
            //game operation
            for ($i = 0; $i < $totalNumUsed - 1 ; $i++){
                $output .= "(";
            }
            $output .= "{$firstNum} ";
            for ($i = 0; $i < $totalNumUsed ; $i++){
                $operation = $gameOperStackSolution->pop();
                $number = $gameNumStackSolution->pop();
                $output .= "{$operation} {$number}) ";
            }
            $output .= "\n\n------\n\n";
            echo $output;
          
        }


    }

?>