<?php
    class GameGenerator{
        const FIRST_LIST = array (25, 50, 75, 100);
        const SECOND_LIST = array (1, 1, 2, 3, 3, 4, 4, 5, 5, 6, 
                                   6, 7, 7, 8, 8, 9, 9, 10, 10);
        //stores the games
        private $gameList;
        
        function __construct($numGames){
            $this->gameList = array ();
            //Generate Games depending on the input
            for ($i = 0; $i < $numGames; $i++){
                array_push($this->gameList, $this->gameGenerator());
            }            
        }
        //returns a set and a target(complete game)
        private function gameGenerator(){
            $firstSetSize = rand (1, 4);
            $secondSetSize = 6 - $firstSetSize;
            //Generate a set
            $firstSet = $this->setGenerator(self::FIRST_LIST, $firstSetSize);
            $secondSet = $this->setGenerator(self::SECOND_LIST, $secondSetSize);
            $gameSet = array_merge($firstSet, $secondSet);
            //Generate a target
            $gameTarget = rand(101, 999);
            return array($gameSet, $gameTarget);
        }

        //returns a random list
        private function setGenerator($list, $size){
            $set = array();
            for ($i = 0; $i < $size; $i++){
                $index = array_rand ($list, 1);
                $set[$i] = $list [$index];
                unset ($list [$index]);
            }
            return $set;
        }
        
        function getGames(){
            return ($this->gameList);
        }
    }
?>