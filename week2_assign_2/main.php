#!/usr/bin/php
<?php
    require_once "GameGenerator.php";
    require_once "GameSolver.php";
    require_once "Stack.php";
    require_once "GameOutput.php";
    //user input
    $numGames = readline ("How many games would you like me to play today? \n");    
    if (!ctype_digit ($numGames)){
        exit ("Wrong input, try again\n");
    }
    $gameGenerator = new GameGenerator ($numGames);
    //$gameList = array(array(array(25, 9, 1, 7, 2, 3), 872));
    //$gameList = array(array(array(25, 9, 1,), 250));
    $gameList = $gameGenerator->getGames ();
    //solve and output each game
    for ($i = 0; $i < $numGames; $i++){
        $gameSolver = new GameSolver($gameList[$i][0], $gameList[$i][1]);
        //var_dump($gameSolver->getNumStackSolution());
        //var_dump($gameSolver->getOperStackSolution());
        $gameOutput = new GameOutput($i, $gameList[$i][0], $gameList[$i][1] , $gameSolver);
        
        //echo "TARGET::{$gameList[$i][1]}";
    }
    
?>