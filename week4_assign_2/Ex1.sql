CREATE database patient_claims; 
USE patient_claims;

CREATE TABLE Claims (
    claim_id INT NOT NULL AUTO_INCREMENT,
    patient_name VARCHAR(30) NOT NULL,
    PRIMARY KEY(claim_id)
);

CREATE TABLE Defendants (
    claim_id INT NOT NULL,
    defendant_name VARCHAR(30) NOT NULL,
    FOREIGN KEY (claim_id)
        REFERENCES Claims (claim_id)
        ON DELETE CASCADE
);

CREATE TABLE ClaimStatusCodes (
    claim_status VARCHAR(2) NOT NULL,
    claim_status_desc VARCHAR(30) NOT NULL UNIQUE,
    claim_seq INT NOT NULL UNIQUE,
    PRIMARY KEY(claim_status)
);

CREATE TABLE LegalEvents (
    claim_id INT NOT NULL,
    defendant_name VARCHAR(30) NOT NULL,
    claim_status VARCHAR(2) NOT NULL,
    change_date DATE NOT NULL,
    FOREIGN KEY (claim_id)
        REFERENCES Claims (claim_id)
        ON DELETE CASCADE,
    FOREIGN KEY (claim_status)
        REFERENCES ClaimStatusCodes (claim_status)
); 
USE patient_claim;
INSERT INTO Claims VALUES (NULL, 'Bassem Dghaidi'); 
INSERT INTO Claims VALUES (NULL, 'Omar Breidi'); 
INSERT INTO Claims VALUES (NULL, 'Marwan Sawwan'); 

INSERT INTO ClaimStatusCodes VALUES('AP', 'Awaiting review panel', 1); 
INSERT INTO ClaimStatusCodes VALUES('OR', 'Panel opinion rendered', 2); 
INSERT INTO ClaimStatusCodes VALUES('SF', 'Suit filed', 3); 
INSERT INTO ClaimStatusCodes VALUES('CL', 'Closed', 4);  

INSERT INTO Defendants VALUES (1, 'Jean Skaff');
INSERT INTO Defendants VALUES (1, 'Elie Meouchi');
INSERT INTO Defendants VALUES (1, 'Radwan Sameh');
INSERT INTO Defendants VALUES (2, 'Joseph Eid');
INSERT INTO Defendants VALUES (2, 'Paul Syoufi');
INSERT INTO Defendants VALUES (2, 'Radwan Sameh');
INSERT INTO Defendants VALUES (3, 'Issam Awwad');

INSERT INTO LegalEvents VALUES(1, 'Jean Skaff', 'AP', '2016-01-01'); 
INSERT INTO LegalEvents VALUES(1, 'Jean Skaff',  'OR', '2016-02-02'); 
INSERT INTO LegalEvents VALUES(1, 'Jean Skaff',  'SF', '2016-03-01'); 
INSERT INTO LegalEvents VALUES(1, 'Jean Skaff', 'CL', '2016-04-01'); 
INSERT INTO LegalEvents VALUES(1, 'Radwan Sameh', 'AP', '2016-01-01'); 
INSERT INTO LegalEvents VALUES(1, 'Radwan Sameh', 'OR', '2016-02-02'); 
INSERT INTO LegalEvents VALUES(1, 'Radwan Sameh', 'SF', '2016-03-01'); 
INSERT INTO LegalEvents VALUES(1, 'Elie Meouchi', 'AP', '2016-01-01'); 
INSERT INTO LegalEvents VALUES(1, 'Elie Meouchi', 'OR', '2016-02-02'); 
INSERT INTO LegalEvents VALUES(2, 'Radwan Sameh', 'AP', '2016-01-01'); 
INSERT INTO LegalEvents VALUES(2, 'Radwan Sameh', 'OR', '2016-02-01'); 
INSERT INTO LegalEvents VALUES(2, 'Paul Syoufi', 'AP', '2016-01-01'); 
INSERT INTO LegalEvents VALUES(3, 'Issam Awwad', 'AP', '2016-01-01');

SELECT  # The patient name for each min
    Claims.claim_id, patient_name, claim_status
FROM # The claim status of each min
    (SELECT 
        MinCurrIdStatuses.claim_id, CSC.claim_status
    FROM # The min of the current statuses for each claim
        (SELECT 
        claim_id, MIN(TotalDefendants) AS MinStatus
    FROM # All the current statuses of the defendants for each claim
        (SELECT 
        claim_id, COUNT(defendant_name) AS TotalDefendants
    FROM
        LegalEvents
    GROUP BY claim_id , defendant_name) AS DefendantsCurrentStatus
    GROUP BY claim_id) AS MinCurrIdStatuses
    JOIN ClaimStatusCodes AS CSC ON MinCurrIdStatuses.MinStatus = CSC.claim_seq) AS MinCurrStatus
        JOIN
    Claims ON Claims.claim_id = MinCurrStatus.claim_id;