#!/usr/bin/php
<?php
//changes directory to working direcoty
chdir(getcwd());
if(!file_exists(".git")){
    exit("There's no repository in the current directory \n");
}

$args = Parser($argv);
//array of logs with specific author, email and commit message if possible
$logs = explode(PHP_EOL, Shell_exec("git log --pretty='%H - %an - %cd - %s' \
--author='\(" . @$args['a']. "\).*<.*\(" . @$args['e'] . "\).*>' -i --grep='". @$args['m'] ."'"));
array_pop($logs);
$id = 1;
//Check every log
$num_logs = sizeof($logs);
foreach($logs as $log){    
    if(Checker($log, $args)){
        echo $id . ":: " . $log . "\n";
        $id++;
    }
}

//log validation
function Checker($log, $args){
    $date = explode('-', @$args['d']);
/** if(is_numeric($date)){
        $day_num = $date[0];
    }else{
        $day_alph = $date[0];
    }
    **/
    $day = @$date[0];
    $month = @$date[1];
    $year = @$date[2];
    
    $log_timestamp = strtotime(preg_match("/-.*?\s-(.*)-/i", $log));
   
    //check for data, time  or timestamp if possible
    if(preg_match("/-.*?-\s([" . @$day . "]+)\S*\s([" . @$month . "]+)\S*\s\S*\s(" . @$args['t'] .
     ")\S*\s(" . @$year . ")/i", $log) || @$args['s'] == $log_timestamp) {

    echo(@$args['s'] . "   " . $log_timestamp . "\n");
        return TRUE;
    }else {
        return FALSE;
    }

}

//parses the command
function Parser($argv){
    //array of possible options
    $avail_opts = ['a', 'e', 't', 'd', 's', 'm'];
    //get rid of the filename
    unset($argv[0]);
    //odd -> option missing arguments
    if(sizeof($argv) % 2 != 0){
        exit("Error : double check your arguments!\n");
    }
    $param_size = sizeof($argv);
    //arguments used
    $arguments = array();
    //Fill the array with the used arguments
    for ($i = 1; $i <= $param_size; $i += 2){
        $opt = $argv[$i];
        $opt_tag = $opt[1];
        $argument = $argv[$i+1];
        //option validation
        if($opt[0] == '-' && strlen($opt) == 2 && in_array($opt_tag, $avail_opts)){
            //store the argument 
            $arguments[$opt_tag] = $argument;
            //everyone option can be used once only
            unset($avail_opts[array_search($opt_tag, $avail_opts)]);
        }else{
            exit("Error : check your options!\n");
        }    
    }
    return $arguments;
}


?>