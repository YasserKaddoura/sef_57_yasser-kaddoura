/**
 * @file Reverse engineering a bad password checker
 * @author Yasser Kaddoura
 */
/**
 * A bad implementation of a function 
 * that checks for a good password
 * 
 * @param {String} password - The password input
 */
function checkPass(password, target) { 
    password = document.getElementById("password").value;
    target = document.getElementById("target").value;
    var total = 0 ;
    var charlist = "abcdefghijklmnopqrstuvwxyz";

    // Iterate through every character of password
    for (var i = 0; i < password.length; i++) {
        // Get the character ot index i
        var countone = password.charAt(i);
        // Get the index of character countone and then increment it by 1
        var counttwo = (charlist.indexOf(countone));
        counttwo++;
        // Multiply total by 17 then add to it counttwo
        total *= 17;
        total += counttwo; 
    }
    console.log(total);
    if (total == target) { 
        console.log("TRUE");
        // setTimeout ("location.replace('index.php?password=" + password
        //             + "' ) ; " , 0)
    }
    else {
        console.log("FALSE");
        // alert ("Sorry, but the password was incorrect.");
    }
    console.log(total);
}

/**
 * Reverse engineering that naive password checker
 * 
 * @param {Array} passwords - the number supplied by checkPass
 * @param {Integer} current - the index of the password in the array
 * @param {Integer} given - the remaining number to extract the password from
 * @returns {String} the password needed to login
 */
function recursive2(passwords, current, given) {
    // return if the given number is 0
    if (given == 0) {
        console.log(passwords[current]);
        return;
    }
    var charlist = "abcdefghijklmnopqrstuvwxyz";

    // Decriment one to get ready for character extraction
    given--;
    // Keep a copy for the current password just in case
    var temp1 = passwords[current];
    // Get the position of the character regarding the alphabet
    var charPos = given % 17;
    
    // If the character index is less than 8 and the remaining number is less than 17 insert a new password
    if (charPos <= 8 && given >= 17) {
        // The other possible character position
        var charPost = charPos + 17;
        // Insert the character at first of the string        
        passwords[current] = charlist[charPost] + passwords[current];
        
        var temp = given - charPost;
        temp /= 17;
        
        // Add the new password the array
        passwords.push(passwords[current]);
        // Get its position in the array
        current = passwords.length-1;
        // Extract the remaining characters for the password or discover a new one
        recursive2(passwords, current, temp);
    }
    // Insert the character at first of the string
    passwords[current] = charlist[charPos] + temp1;        

    given -= charPos;
    given /= 17;
    // Extract the remaining characters for the password or discover a new one
    recursive2(passwords, current, given);
}

