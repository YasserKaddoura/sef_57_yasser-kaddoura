// A list of the month names
const monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"];
// A list of the day names
const dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thurdsay",                         "Friday", "Saturday"];
/**
 * Add an item to the do list depending on the input of the user
 */
function addToList()
{   
    // Title of the item provided by the user
    var title = document.getElementById('title').value;
    // Description of the item provided by the user
    var description = document.getElementById('description').value;
    // Current date
    var now = new Date();
    var dateFormat = "added: " + dayNames[now.getDay()] + ", " + monthNames                       [now.getMonth()] + " " + now.getDate() + " " +                              now.getFullYear() + " " + now.getHours() + ":"                              + now.getMinutes();
    // Checks if the input is valid
    if (inputValidator(title) && inputValidator(description)) {
        addElement(title, description, dateFormat);
    } else {
        alert('Invalid input');
    }
}

/**
 * Checks if the string is empty
 * 
 * @param {string} string - the string to be validated
 * @returns {boolean} - true if the input is valid - false if it's not
 */
function inputValidator(string)
{
    return string.trim().length != 0;
}

/**
 * Create a new item and insert it to the list
 * 
 * @param {string} title - the title of the item given from the user
 * @param {string} description - the description of the item given from the user
 * @param {string} dateFormat - the desired dateformat for current time
 * 
 */
function addElement(title, description, dateFormat)
{   
    // Get the element from DOM
    var toDoList = document.getElementById("toDoList");
    // Populate a new item list using html
    var newItem = '<li><div class="text"><h3 class="item-title">' + title + 
                  '</h3><p class="item-time">' + dateFormat + 
                  '</p><p class="item-description">' + description + 
                  '</p></div><div><span class="divider"></span><button class="item-delete" onclick="deleteMe(this)"></button></div></li>';
    // Insert the new item at the start of the list
    toDoList.innerHTML = newItem + toDoList.innerHTML;
    // Update the local storage
    updateLocalStorage();
}
/**
 * Deletes an item from the list
 * 
 * @param {Object} - the clicked button element
 */
function deleteMe(item)
{
    var toDoList = document.getElementById("toDoList");
    // Delete the parent element of the button which is the item from the list
    toDoList.removeChild(item.parentElement);
    // Updaet the local storage
    updateLocalStorage();        
}

/**
 * Updates the local storage
 */
function updateLocalStorage() {
    var toDoList = document.getElementById('toDoList');
    localStorage.setItem('toDoList', toDoList.innerHTML);
}

/**
 * Loads the items from the local localStorage
 */
window.onload = function ()
{
    // Check if the property is available
    if (localStorage.toDoList) {
        // Load the items into the available list
        document.getElementById("toDoList").innerHTML = localStorage.getItem('toDoList');
    }
}
