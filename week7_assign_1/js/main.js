/**
 * SEF Summarizer
 * 
 * @author : Yasser Kaddoura
 */

/**
 * UserInput constructor function
 * 
 * @param {String} url the url of the article
 * @param {String} numSentences the number of sentences for the summary
 */
var UserInput = function(url, numSentences) {
    this.url = url;
    this.numSentences = numSentences;
}

/**
 * Checks if the user didn't insert anything
 * 
 * @return {Boolean} true if the url argument of the object is empty else false
 */
UserInput.prototype.isEmpty = function() {
    // Check if the user input is empty
    if (this.url === "") {
        return true;
    }
    return false;
}

/**
 * Checks if the url given by the user is valid
 * 
 * @return {Boolean} true if any of the arguments of the object is empty else false
 */
UserInput.prototype.isValidURL = function() {
    // Regular expression for a valid URL
    var urlPattern = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    // Test the previous RE on the url
    if(urlPattern.test(this.url)) {
        return true;
    }
    return false;
}

/**
 * Get the user input user and return the summary
 * 
 * @param {String} userInput the user input
 */
var InputHandler = function(userInput) {
    this.userInput = userInput;
    // Values for the article extraction
    this.func = this.getArticle;
    this.content = "url=" + userInput.url + "&method=GETDOM";
    // Start loading
    this.startLoading();
    // Get the article content
    this.httpRequest();
}

/**
 * Manages the communication with the server via XMLHTTPRequest
 */
InputHandler.prototype.httpRequest = function() {
    // Instance of the object
    var myInputHandler = this;
    // Instantiating a xmlhttprequest object
    var xhhtp = new XMLHttpRequest();
    // Setting the method and the script for the request
    xhhtp.open("POST", "webCaller.php", true);
    // Setting the header of the request
    xhhtp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // Access the onreadystatechange event for the XMLHttpRequest object
    xhhtp.onreadystatechange = function() {
        if(xhhtp.readyState == 4 && xhhtp.status == 200) {
            // Get the data returned
            var returnedData = xhhtp.responseText;
            // execute the function
            myInputHandler.func(returnedData);
        }
    };
    // Sends the request to the PHP script
    xhhtp.send(this.content);
}

/**
 * Get the title and the text of the article
 * 
 * @param {String} domString the data recieved from the server
 */
InputHandler.prototype.getArticle = function(domString) {
    // Instantiate a dom parser
    var parser = new DOMParser();
    // Get the element that we will work on
    var main = parser.parseFromString(domString, "text/html").getElementsByTagName('main')[0];
    // Get the title of the article
    var header = main.getElementsByTagName('h1')[0];
    var title = this.getContent(header);
    // Get the paragraphs of the article
    var pElements =  main.getElementsByTagName('p');
    var size = pElements.length;
    var text = "";
    for (var i = 0; i < size; i++) {
        text += this.getContent(pElements[i]);
    }
    // Change the stage, content and the function for the object
    this.stage = "APICALL";
    this.content = "text=" + text +"&title=" + title + "&numSentences=" + 
                    this.userInput.numSentences + "&method=APICALL";
    this.func = this.summarize;
    // Updating the title
    sumTitle.innerHTML = title;
    // Get the image source from 
    var figure =  main.getElementsByTagName('figure')[0].getElementsByTagName('img')[0].src;
    // Update the image
    document.getElementById('summary-figure').src = figure;
    this.httpRequest();
}
/**
 * Set the text of the summary and stop the loading
 */
InputHandler.prototype.summarize = function(result) {
    sumText.innerHTML = result;
    this.stopLoading();    
}

/**
 * get the value stored inside the html element
 * 
 * @param {Object} domElement an html element
 * @return {String} the value of the element
 */
InputHandler.prototype.getContent = function(domElement) {
    return domElement.innerHTML.replace(/&nbsp;/g,' ');
}

/**
 * Loads a loading animation and disables the page
 */
InputHandler.prototype.startLoading = function() {
    // Disable the page
    var div= document.createElement('div');
    div.id += 'overlay';
    document.body.appendChild(div);
    // Enable the loading animation
    loader.className = 'loader-start';
}

/**
 * Stops the loading animation and enables the page
 */
InputHandler.prototype.stopLoading = function() {
    // Enable the page
    document.body.removeChild(document.getElementById('overlay'));
    // Disable the loading animation 
    loader.className = 'loader-pause';
}
/**
 * GLOBAL VARIABLES
 */
var button = document.getElementById("button");
var loader = document.getElementById("loader");
var sumTitle = document.getElementById("summary-title");
var sumText = document.getElementById("summary-text");

/**
 * Takes the information from the DOM HTMLElements
 * and returns a UserInput with the information
 * stored in it as properties
 *
 * @return {UserInput} the input of the user stored in a UserInput object
 */
function getUserInput() {
    // Get the values from the DOM Elements
	var url = document.getElementById('url').value;
	var taskDescription = document.getElementById('num-sentences').value;

    // Return a new User Input
	return new UserInput(url, taskDescription);
};

/**
 * Summarizes the article
 * 
 * @param {UserInput} userInput 
 */
function summarize(userInput) {
    var myInputHandler = new InputHandler(userInput);
    myInputHandler.httpRequest('GETDOM');
};



/**
 * EVENT HANDLERS
 */ 

/**
 * addButton Click Event Handling
 * Checks if the user input is valid before
 * invoking instantiating a myInputHandler
 */
button.addEventListener('click', function(event) {
	// Get user input
	var myUserInput = getUserInput();
	// Check the user input if empty
	if (!myUserInput.isEmpty()) {
        // Check the user input if valid
        if (myUserInput.isValidURL()) {
            var myInputHandler = new InputHandler(myUserInput);
        } else {
            alert("Invalid URL");
        }
	} else {
		alert("The URL shouldn't be empty");        
	}
});