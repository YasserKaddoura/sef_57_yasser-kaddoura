<?php
require_once 'config.php';


/**
  * Get the DOM of a certain webage
  *
  *@param String - to be processed article
  */
function getDOM($url) {
    
    $ch = curl_init($url);
    //  Return the response as a string instead of outputting it to the screen
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Get the page after redirection
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    // Get the result
    $page = curl_exec($ch);
    curl_close($ch);
    echo $page;
}

/**
  * Get the summzarization of the text by using the aylien API using GET
  */
function apiCall($params) {
    // Ready a url to be sent
    $url = ENDPOINT . 'summarize?text=' . urlencode($params['text']) .
                                 '&title=' . urlencode($params['title']) . 
                                 '&sentences_number=' . urlencode($params['sentences_number']);
    $ch = curl_init();

    curl_setopt_array($ch, array(
                    //  Return the response as a string instead of outputting it to the screen    
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => $url,
));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Accept: application/json',
        'X-AYLIEN-TextAPI-Application-Key: ' . KEY,
        'X-AYLIEN-TextAPI-Application-ID: '. ID
    ));
    $response = curl_exec($ch);
    curl_close($ch);
    $decodedResponse = json_decode($response);
    // Returning back the sentences to the browser
    echo implode("<br>", $decodedResponse->sentences);
}

// Checks the post arguments
if (isset($_POST['method'])) {
    // Get the DOM from the article
    if ($_POST['method'] == 'GETDOM') {
        if (isset($_POST['url'])) {
            getDOM($_POST['url']);
        } // Send the article to be summarized
    } elseif ($_POST['method'] == 'APICALL') 
        if (isset($_POST['title']) && isset($_POST['text'])
            && isset($_POST['numSentences'])) {
        // Pass the title, text and number of sentences to the function
        apiCall(array("title" => $_POST['title'], 
                      "text" => $_POST['text'],
                      "sentences_number" => $_POST['numSentences']));
    }

}

?>