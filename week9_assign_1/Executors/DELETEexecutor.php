<?php

require_once './Tools/MySQLWraper.php';

class DELETEexecutor
{

    /**
     * Private constructor for static class 
     */
    private function __construct()
    {
    }


    /**
     * Delete row/s from table
     *
     * @param Input $input Input
     *
     * @return void
     */
    public static function execute($input)
    {
        $elements = $input->getElements();
        $SQL = self::_deleteGenerator($input);
        MySQLWraper::transaction();
        $lastTable = $elements[count($elements)-1]['table'];                   
        
        $input->setResult(self::_selectGenerator($input));
        $result = MySQLWraper::delete($SQL);

        if ($result === false) {
            $input->setCodeDes(
                400,
                "Bad Input or Can't Delete because of foreign key constrain"
            );
        } else if ($result === 0) {
            $input->setDescription(
                "No rows got deleted on table {$lastTable}"
            ); 
        } else {
            $input->setDescription(
                "Successful deletion on table {$lastTable}"
            );
        }
        MySQLWraper::commit();
    }

    /**
     *
     */
    private static function _deleteGenerator($input)
    {
        $tables = $input->getElements();
        $lastTable = $tables[count($tables)-1]['table'];
        $SQL = "DELETE $lastTable FROM ";

        $SQL .= self::_getJoins($tables);
        $SQL .= self::_getConditions($tables);

        return $SQL;
    }

    
    /**
     * Generates a query
     * 
     * @param Input $input Input Object
     *
     * @return String string for the query
     */
    private static function _selectGenerator($input)
    {   
        $tables = $input->getElements();
        $lastTable = array_pop($tables);

        $SQL = "SELECT *";

        $elements = $lastTable['elements'];
 
        $SQL .= " FROM {$lastTable['table']}";
        // Checks for join
        if (isset($tables)) {
            $SQL .= self::_getJoins($tables);
        }
        $SQL .= self::_getConditions($input->getElements());

        $result = MySQLWraper::select($SQL);
        return $result;

    }  

    /**
     * Adds join string to the query
     *
     * @param Array $tables List of tables to be joined with
     *
     * @return String join String
     */
    private static function _getJoins($tables)
    {
        $SQL = '';
        foreach ($tables as $table => $value) {
            $tableName = $value['table'];
            $SQL .= $tableName . ' JOIN ';
        }
        return substr($SQL, 0, -6);
    }

    /**
     * Returns the updatedValues needed for the query
     *
     * @param Array $updatedValues List of tables
     *
     * @return String updated values
     */
    private static function _getValues($updatedValues)
    {
        $SQL = '';
        foreach ($updatedValues as $key => $value) {
                $SQL .= $key . " = '" . $value . "' AND ";
        }
            return substr($SQL, 0, -5);
    }

    /**
     * Returns the attributes needed for the query
     *
     * @param Array $tables List of tables
     *
     * @return String needed conditions for the query
     */
    private static function _getConditions($tables)
    {
        $SQL = '';
        foreach ($tables as $table => $elements) {
            foreach ($elements['elements'] as $att => $value) {
                if (Validator::attExists($elements['table'], $att)) {
                    $SQL .= $att . " = '" . $value . "' AND ";
                }
            }
        }
        if (empty($SQL)) {
            return '';
        } else {
            return ' WHERE ' . substr($SQL, 0, -5);
        }
    }
}
   