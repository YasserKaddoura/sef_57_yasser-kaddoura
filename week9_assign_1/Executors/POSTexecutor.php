<?php

require_once './Tools/MySQLWraper.php';

class POSTexecutor
{

    /**
     * Private constructor for static class 
     */
    private function __construct()
    {
    }


    /**
     * Insert a record into the table
     *
     * @param Input $input Input
     *
     * @return void
     */
    public static function execute($input)
    {
        $elements = $input->getElements();
        $SQL = self::_insertGenerator($elements);
        // Executes the query and return the id of the newly inserted row
        MySQLWraper::transaction();
        $result = MySQLWraper::insert($SQL);
        if ($result === false) {
            $input->setCodeDes(400, 'Bad Input');
        } else {
            $input->setResult(self::_selectGenerator($result, $elements));
            $input->setDescription(
                "Successful insertion on table {$elements['table']}"
            );
        }
        MySQLWraper::commit();
    }
    
    /**
     *
     */
    private static function _insertGenerator($elements)
    {
        $SQL = "INSERT INTO {$elements['table']}";
        $atts = '(';
        $values = '(';
        foreach ($elements['atts&values'] as $att => $value) {
            $atts .= "$att,";
            $values .= "'$value',";            
        }
        // Substituting the last comma with a closing bracket
        $atts = substr($atts, 0, -1) . ')';
        $values = substr($values, 0, -1) . ')';

        $SQL .= "$atts VALUES $values";
        return $SQL;
    }

    /**
     * Generates a formated string for the response
     *
     * @param Integer $id       id of the newly inserted row
     * @param Array   $elements elments for CRUD
     *
     * @return Array Inserted row
     */
    private static function _selectGenerator($id, $elements)
    {
        // TODO : HANDLE COMPOSET
        // Get the primary key column of the table
        $primaryKeyColumn = MySQLWraper::select(
            "SHOW KEYS FROM {$elements['table']} WHERE Key_name = 'PRIMARY'"
        );
        // First primary key column name 
        $primaryKey = $primaryKeyColumn[0]['Column_name'];
        // Get the inserted row
        $result = MySQLWraper::select(
            "SELECT * FROM {$elements['table']} WHERE {$primaryKey} = $id"
        );
        return $result;

        // return $SQL;
    }

}