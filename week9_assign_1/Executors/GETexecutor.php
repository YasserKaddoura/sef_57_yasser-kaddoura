<?php

require_once './Tools/MySQLWraper.php';

class GETexecutor
{

    /**
     * Private constructor for static class 
     */
    private function __construct()
    {
    }
    
    /**
     * Makes a select query from the tbale
     *
     * @param Input $input Input
     *
     * @return void
     */
    public static function execute($input)
    {
        $SQL = self::_selectGenereator($input);
        $result = MySQLWraper::select($SQL);
        if ($result === false) {
            $input->setCodeDes(400, 'Bad Input');
        } else {
            $input->setResult($result);
            if ($input->isOkay()) {
                $input->setDescription(
                    "Successful selection"
                );
            }
        }
    }
    
    /**
     * Generates a query
     * 
     * @param Input $input Input Object
     *
     * @return String string for the query
     */
    private static function _selectGenereator($input)
    {   
        $tables = $input->getElements();
        $lastTable = array_pop($tables);

        $SQL = "SELECT ";

        $elements = $lastTable['elements'];
        if (isset($elements['fields'])) {
            $SQL .= $elements['fields'];
        } else {
            $SQL .= '*';
        }
        $SQL .= " FROM {$lastTable['table']}";
        // Checks for join
        if (isset($tables)) {
            $SQL .= self::_getJoins($tables);
        }
        $SQL .= self::_getConditions($input->getElements());
        if (isset($elements['orderby'])) {
            $SQL .= self::_getOrderBy($lastTable, $input);
        }

        if (isset($lastTable['limit'])) {
            $SQL .= ' LIMIT ' . $lastTable['limit'];
        }

        return $SQL;

    }   
    /**
     * Adds join string to the query
     *
     * @param Array $tables List of tables to be joined with
     *
     * @return String join String
     */
    private static function _getJoins($tables)
    {
        $SQL = '';
        foreach ($tables as $table => $value) {
            $tableName = $value['table'];
            $SQL .= ' JOIN ' . $tableName;
        }
        return $SQL;
    }
    /**
     * Returns the attributes needed for the query
     *
     * @param Array $tables List of tables
     *
     * @return String needed conditions for the query
     */
    private static function _getConditions($tables)
    {
        $SQL = '';

        foreach ($tables as $table => $elements) {
            if (!isset($elements['elements'])) {
                continue;
            }
            foreach ($elements['elements'] as $att => $value) {
                if (Validator::attExists($elements['table'], $att)) {
                    $SQL .= $att . " = '" . $value . "' AND ";
                }
            }
        }
        if (empty($SQL)) {
            return;
        } else {
            return ' WHERE ' . substr($SQL, 0, -5);
        }
    }
    // TODO ASCEND + DESC CAN'T BE ALONE
    /**
     * Returns string for order by string ofr query
     *
     * @param Array $table table attributes
     * @param Input $input Input Object
     *
     * @return String needed orderby for the query
     */
    private static function _getOrderBy($table, $input)
    {
        $elements = $table['elements'];
        $SQL = ' ORDER BY ';
        $values = explode('-', $elements['orderby']);
        $total = count($values);
        if ($total === 0
            || $total > 2
        ) {
            $input->setCodeDes(
                410,
                'Warning: Limit attribute is invalid.'
            );
            return '';
        }
        // Attribute ordered by
        if (Validator::attExists($table['table'], $values[0])) {
            $SQL .= $values[0];                
        } else {
            $input->setCodeDes(
                410, 
                "Warning:OrderBy-$values[0] is not an attribute in {$table['table']}"
            );
            return '';
        }
        if ($total == 2) {

            if (strcmp($values[1], 'asc')
                || strcmp($values[1], 'desc')
            ) {
                $SQL .= ' ' . $values[1];
            } else {
                $input->setCodeDes(
                    410,
                    "Warning: OrderBy - $values[1] is invalid"
                );
                return '';
            }
        }
        return $SQL;

    }
}