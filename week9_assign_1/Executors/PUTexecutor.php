<?php

require_once './Tools/MySQLWraper.php';

class PUTexecutor
{

    /**
     * Private constructor for static class 
     */
    private function __construct()
    {
    }


    /**
     * Insert a record into the table
     *
     * @param Input $input Input
     *
     * @return void
     */
    public static function execute($input)
    {
        $elements = $input->getElements()['conditions'];
        $SQL = self::_updateGenerator($input);

        MySQLWraper::transaction();
        $result = MySQLWraper::update($SQL);
        $lastTable = $elements[count($elements)-1]['table'];                    
        if ($result === false) {
            $input->setCodeDes(400, 'Bad Input');
        } else if ($result === 0) {
            $input->setDescription(
                "No rows got updated on table {$lastTable}"
            ); 
        } else {
            $input->setResult(self::_selectGenerator($lastTable));
            $input->setDescription(
                "Successful update on table {$lastTable}"
            );
        }
        MySQLWraper::commit();
    }

        
    /**
     * Generated a query for update
     *
     * @param Input $input Input object
     *
     * @return String SQL query for update
     */
    private static function _updateGenerator($input)
    {
        $tables = $input->getElements()['conditions'];

        $SQL = "UPDATE ";

        $SQL .= self::_getJoins($tables) . ' SET ' ;
        $SQL .= self::_getUpdatedValues($input->getElements()['updatedValues']);
        $SQL .= self::_getConditions($tables);

        return $SQL;
    }

    /**
     * Select all the rows that got updated using last_updated
     *
     * @param String $lastTable elments for CRUD
     *
     * @return Array update rows
     */
    private static function _selectGenerator($lastTable)
    {
        // TODO : HANDLE COMPOSET
        // Get the primary key column of the table
        $now = date('Y-m-d H:i:s');

        $result = MySQLWraper::select(
            "SELECT * FROM {$lastTable} WHERE last_update = '{$now}'"
        );
        return $result;
    }

    /**
     * Adds join string to the query
     *
     * @param Array $tables List of tables to be joined with
     *
     * @return String join String
     */
    private static function _getJoins($tables)
    {
        $SQL = '';
        foreach ($tables as $table => $value) {
            $tableName = $value['table'];
            $SQL .= $tableName . ' JOIN ';
        }
        return substr($SQL, 0, -6);
    }

    /**
     * Returns the updatedValues needed for the query
     *
     * @param Array $updatedValues List of tables
     *
     * @return String updated values
     */
    private static function _getUpdatedValues($updatedValues)
    {
        $SQL = '';
        foreach ($updatedValues as $key => $value) {
                $SQL .= $key . " = '" . $value . "', ";
        }
            return substr($SQL, 0, -2);
    }

    /**
     * Returns the attributes needed for the query
     *
     * @param Array $tables List of tables
     *
     * @return String needed conditions for the query
     */
    private static function _getConditions($tables)
    {
        $SQL = '';
        foreach ($tables as $table => $elements) {
            foreach ($elements['elements'] as $att => $value) {
                if (Validator::attExists($elements['table'], $att)) {
                    $SQL .= $att . " = '" . $value . "' AND ";
                }
            }
        }
        if (empty($SQL)) {
            return '';
        } else {
            return ' WHERE ' . substr($SQL, 0, -5);
        }
    }
}