<?php

require_once 'Tools/MySQLWraper.php';
require_once 'Tools/Validator.php';
require_once 'Tools/Respond.php';

require_once 'Executors/POSTexecutor.php';
require_once 'Executors/GETexecutor.php';
require_once 'Executors/PUTexecutor.php';
require_once 'Executors/DELETEexecutor.php';


require_once 'Parsers/GETParser.php';
require_once 'Parsers/POSTParser.php';
require_once 'Parsers/DELETEParser.php';
require_once 'Parsers/PUTParser.php';

// TODO pagination-aliases-authorization-limit

MySQLWraper::connect();
$myInput = new Input;

if (!$myInput->isOkay()) {
    echo Respond::response($myInput);
    exit();
}
// Class name for the parser
$parserClass = $myInput->getMethod() . 'Parser';
if (!class_exists($parserClass)) {
    $myInput->setCodeDes(500, 'Problem from our side!');
    echo Respond::response($myInput);
    exit();
}

$parser = new $parserClass($myInput);

if (!$parser->isValid()) {
    echo Respond::response($myInput);
    exit();
}

$executorName = $myInput->getMethod() . 'executor';
$executorName::execute($myInput);

echo Respond::response($myInput);

MySQLWraper::disconnect();
?>