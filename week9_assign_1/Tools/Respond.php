<?php

/**
 * 
 */
class Respond
{
    /**
     * Generates a response to the user
     *
     * @param Array/Input $input 
     *
     * @return {JSON}
     */
    public static function response($input)
    {
        if ($input->isOkay() || $input->isWarning()) {
            $response = self::_goodResponse($input);
        } else {
            $response = self::_badResponse($input);
        }
        return self::_makeResponse($response, $input->isPretty());
    }

    /**
     * Generates a bad request to the user
     *
     * @param Array/Input $input 
     *
     * @return {JSON}
     */
    private static function _goodResponse($input)
    {
        return array('code' => $input->getCode(), 
                     'description' => $input->getDescription(),
                     'result' => $input->getResult(),
            );
        
    }

    /**
     * Generates a good request to the user
     *
     * @param Array/Input $input 
     *
     * @return {JSON}
     */
    private static function _badResponse($input)
    {
        return array('code' => $input->getCode(), 
                     'description' => $input->getDescription(),
            );
    }

    /**
     * Encodes an array to send it back the user
     *
     * @param String $response string to be decoded
     *
     * @return {JSON}
     */
    private static function _makeResponse($response, $pretty = true)
    {   
        if ($pretty === true) {
            return stripslashes(
                json_encode(
                    self::_utf8ize($response),
                    JSON_PRETTY_PRINT
                )
            );
        } else {
            return stripslashes(
                json_encode(
                    self::_utf8ize($response)
                )
            );
        }
    }
    /**
     * Encodes not utf8 strings to utf8
     *
     * @param String $result the string to be encoded
     *
     * @return String pure utf-8 formatted string
     */
    private static function _utf8ize($result)
    {
        if (is_array($result)) {
            foreach ($result as $k => $v) {
                $result[$k] = self::_utf8ize($v);
            }
        } else if (is_string($result)) {
            return utf8_encode($result);
        }
        return $result;
    }
}
    