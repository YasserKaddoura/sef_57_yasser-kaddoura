<?php


class Validator
{
    /**
     * Suppoerted Methods
     */
    private static $_methods = array(
        'GET',
        'POST', 
        'DELETE', 
        'PUT',
    );

    /**
     * Suppoerted tables for Creating tuples
     */
    private static $_tablesPOST = array(
        'customer',
        'address', 
        'film', 
        'actor',
    );
    /**
     * Suppoerted tables for Reading tuples
     */
    private static $_tablesGET = array('*');
    /**
     * Suppoerted tables for Updating tuples
     */
    private static $_tablesPUT = array(
        'customer',
        'address', 
        'film', 
        'actor',
        'store',
        'rental',
        'payment',
        'staff',
    );
    /**
     * Suppoerted tables for Deleting tuples
     */
    private static $_tablesDELETE = array(
        'customer',
        'address', 
        'film',        
    );
    /**
     * Suppoerted Methods
     */
    private static $_features = array(
        'limit',
        'pretty', 
        'fields', 
        'orderby',
    );
    /**
     * Constructor - private cause the class is a singleton
     */
    private function __construct()
    {
    }

    /**
     * Checks if the table exist
     *
     * @param - $table Table name
     *
     * @return Boolean True if table exists else false
     */
    public static function tableExists($table)
    {
        return MYSQLWraper::tableExists($table);
    }

    /**
     * Checks if the table is available for API
     *
     * @param String $table  Table used eg. actor-Movies
     * @param String $action Action used
     *
     * @return Boolean
     */
    public static function tableSupported($table, $action)
    {
        // Doesn't support all tables
        $tableList = '_tables' . $action;
        if (!in_array('*', self::$$tableList)) {
            return in_array($table, self::$$tableList);
        }
        return true;
    }

    /**
     * Checks if the attribute exists inside a table
     *
     * @param String $table Table name
     * @param String $att   attribute name     
     *
     * @return Boolean True if attribute exists else false
     */
    public static function attExists($table, $att)
    {
        return MYSQLWraper::attExists($table, $att);
    }

    /**
     * Checks if the feature is supported
     *
     * @param String $feature feature name
     *
     * @return Boolean True if feature exists else false
     */
    public static function featureExists($feature)
    {
        return in_array($feature, self::$_features);
    }
    /**
     * Checks if the request method is supported by the API
     *
     * @param String $method Method used eg. POST-GET
     *
     * @return Boolean
     */
    public static function methodSupported($method)
    {
        return in_array($method, self::$_methods);
    }
}

?>