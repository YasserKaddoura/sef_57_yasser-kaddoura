<?php

/**
 * A wraper class for mysqli class
 *
 * PHP version 5.6
 *
 * @author Yasser Kaddoura <yasserkaddouralearn@gmail.com>
 */

require_once 'config.php';

class MySQLWraper
{
 
    // The database connection
    private static $_connection;
    
    /**
     * Constructor - Makes a connection
     */
    public function __construct()
    {   
        self::$connect();        
    }
    
    /**
     * Connect to the database
     *
     * @return bool FALSE on failure / mysqli MySQLi object instance on success
     */
    public static function connect()
    {
        // Connect if not connected already
        if (!isset(self::$_connection) || !self::$_connection->ping()) {
            self::$_connection = new mysqli(
                DB_HOST, 
                DB_USERNAME, 
                DB_PASSWORD,
                DB_DATABASE
            );   
        }

        if (self::$_connection === false) {
            throw new Exception("Can't establish connection with DB");
        }
    }

    /**
     * Disconnect the database connection
     *
     * @return void
     */
    public static function disconnect()
    {
        self::$_connection->close();
    }

    /**
     * Set the database connection autocommit to false to attain atomicity
     *
     * @return void
     */
    public static function transaction()
    {
        self::$_connection->autocommit(false);
    }

    /**
     * Commit transaction
     *
     * @return void
     */
    public static function commit()
    {   
        if (!self::$_connection->commit()) {
            // Baaaaad  
        }
    }

    /**
     * Checks if the table exist
     *
     * @param - $table Table name
     *
     * @return Boolean True if table exists else false
     */
    public static function tableExists($table)
    {
        $result = self::query("SHOW TABLES LIKE '{$table}'");
        return (mysqli_num_rows($result)) ? true : false;

    }
    
    /**
     * Checks if the attribute exists inside a table
     *
     * @param String $table Attribute name
     * @param String $att   Table name
     *
     * @return Boolean True if attribute exists else false
     */
    public static function attExists($table, $att)
    {

        $result = self::query("SHOW COLUMNS FROM {$table} LIKE '{$att}'");
        return (mysqli_num_rows($result))? true : false;
    }
    
    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param - $query The query string
     *
     * @return Boolean False on failure / array Database rows on success
     */
    public static function query($query)
    {
        $result = self::$_connection->query($query);
        return $result;
    }

    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param - $query The query string
     *
     * @return bool False on failure / array Database rows on success
     */
    public static function select($query)
    {
        $rows = array();
        $result = self::query($query);
        if ($result === false) {
            return false;
        }
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Insert a row into the database (INSERT query)
     *
     * @param - $query The query string
     *
     * @return bool False on failure / integer of the inserted id on success
     */
    public static function insert($query)
    {
        $result = self::query($query);   
        if ($result === false) {
            return false;
        }
        $idInserted = self::$_connection->insert_id;

        return $idInserted;
    }

    /**
     * Delete a row from the database (INSERT query)
     *
     * @param - $query The query string
     *
     * @return bool False on failure / integer of the inserted id on success
     */
    public static function delete($query)
    {
        $result = self::query($query);   

        if ($result === false) {
            return false;
        }
        var_dump(self::$_connection->affected_rows);
        return self::$_connection->affected_rows;
    }
    /**
     * Update a row/s in the database (UPDATE query)
     *
     * @param - $query The query string
     *
     * @return bool False on failure
     */
    public static function update($query)
    {
        $result = self::query($query);   
        if ($result === false) {
            return false;
        }

        return self::$_connection->affected_rows;
    }
    /**
     * Destructor function that closes the connection
     */
    private function __destruct() 
    {
        self::$_disconnect();
    }
}


?>