<?php

require_once 'Parser.php';

/**
 *
 */
class GETParser extends Parser
{

    /**
     * Constructor
     *
     * @param Input $input {Input}
     */
    public function __construct($input)
    {
         parent::__construct($input);
         $this->parse();
    }

    /**
     * Parses the Input
     *
     * @return void
     */
    public function parse()
    {
        parent::parse();
        $elements = $this->_resourceAtt();
        $this->input->setElements($elements);
    }

    // TODO Better way to format the input eg. reduce
    /**
     * Extracts  the attributes for the query
     *
     * @return Array attributes
     */
    private function _resourceAtt()
    {
        $totalSegments = count($this->segments);
        for ($i = 0; $i < $totalSegments; $i++) {
            $segment = $this->segments[$i];
            $section = explode('?', $segment, 2);
            $result[$i]['table'] = $section[0];            
            // table got no elements
            if (!isset($section[1])) {
                $result[$i]['elements'] = null;
            } else {
                $atts = explode('&', $section[1]);
                // Store the attributes in an associative array
                foreach ($atts as $att) {
                    $att1 = explode('=', $att, 2);
                    if (!isset($att1[1])) {
                         $result[$i]['elements'][$att1[0]] = null;
                    } else {
                        $result[$i]['elements'][$att1[0]] = $att1[1];
                    }
                }
            }
        }
        return $result;
    }

    /**
     * GET methods should have their filters on the last segment
     *
     * @return Boolean
     */
    public function isValid()
    {
        $tables = $this->input->getElements();
        $lastTable = array_pop($tables);
        if (!$this->_isValidLastTable($lastTable)) {
            return false;
        }
        // More tables
        if (isset($tables)) {
            return $this->isValidTables($tables);
        }
        return true;
    }

    /**
     * Checks if the last table elements are valid
     *
     * @param String $table Table name
     *
     * @return Boolean True if a valid table else False
     */
    private function _isValidLastTable($table)
    {
        $tableName = $table['table'];
        return $this->isValidTable($tableName)
            && $this->isValidAttributesFeatures($table);
    }

}
?>