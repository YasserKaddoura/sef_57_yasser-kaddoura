<?php

require_once 'Parser.php';

/**
 *
 */
class DELETEParser extends Parser
{

    protected $table;
    private $_elements;
    /**
     * Constructor
     *
     * @param Input $input {Input}
     */
    public function __construct($input)
    {
        parent::__construct($input);
        $this->parse();
    }

        /**
     * Parses the Input
     *
     * @return void
     */
    public function parse()
    {
        parent::parse();
        $elements = $this->_resourceAtt();
        $this->input->setElements($elements);

    }

    /**
     * GET methods should have their filters on the last segment
     *
     * @return Boolean
     */
    public function isValid()
    {
        $tables = $this->input->getElements();
        return $this->isValidTables($tables);
    }
    
    /**
     * Extracts  the conditions for the query
     *
     * @return Array attributes
     */
    private function _getConditions()
    {
        foreach ($this->segments as $segment) {
            $section = explode('?', $segment, 2);
            // table got no elements
            if (!isset($section[1])) {
                $result[$section[0]] = null;
            } else {
                $atts = explode('&', $section[1]);
                // Store the attributes in an associative array
                foreach ($atts as $att) {
                    $att1 = explode('=', $att, 2);
                    if (!isset($att1[1])) {
                         $result[$section[0]][$att1[0]] = null;
                    } else {
                        $result[$section[0]][$att1[0]] = $att1[1];
                    }
                }
            }
        }
        return $result;
    }

        /**
     * Extracts  the attributes for the query
     *
     * @return Array attributes
     */
    private function _resourceAtt()
    {
        $totalSegments = count($this->segments);
        for ($i = 0; $i < $totalSegments; $i++) {
            $segment = $this->segments[$i];
            $section = explode('?', $segment, 2);
            $result[$i]['table'] = $section[0];            
            // table got no elements
            if (!isset($section[1])) {
                $result[$i]['elements'] = null;
            } else {
                $atts = explode('&', $section[1]);
                // Store the attributes in an associative array
                foreach ($atts as $att) {
                    $att1 = explode('=', $att, 2);
                    if (!isset($att1[1])) {
                         $result[$i]['elements'][$att1[0]] = null;
                    } else {
                        $result[$i]['elements'][$att1[0]] = $att1[1];
                    }
                }
            }
        }
        return $result;
    }
}