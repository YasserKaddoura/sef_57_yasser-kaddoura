<?php

require_once 'Parser.php';
require_once 'Tools/Validator.php';
require_once 'Input.php';

/**
 *
 */
class POSTParser extends Parser
{

    protected $table;
    /**
     * Constructor
     *
     * @param Input $input {Input}
     */
    public function __construct($input)
    {
         parent::__construct($input);
         $this->parse();
    }

    /**
     * Parses the Input
     *
     * @return void
     */
    public function parse()
    {
        parent::parse();
    }

    /**
     * Checks if the attributes are inside the table
     *
     * @return Boolean 
     */
    private function _isValidAttributes()
    {
        // $_POST = array(
        //     'first_name' => 'rasha',
        //     'last_name' => 'jello',
        // );
        if (count($_POST) === 0) {
                // Attriubte doesn't exist                
                $this->input->setCodeDes(
                    400,
                    "Not a single value, Seriously?"
                );
                return false;
        }
        foreach ($_POST as $att => $value) {
            if (Validator::attExists($this->table, $att)) {
                // The attribute is valid                        
            } else {
                // Attriubte doesn't exist   
                $this->input->setCodeDes(
                    400,
                    "{$att} is an invalid attribute/s"
                );
                return false;
            }
        }
        $this->extractElements();
        
        return true;
    }

    /**
     * POST method should only have one segment which is 
     * the table that the input should be inserted into
     *
     * @return Boolean
     */
    public function isValid()
    {
        if ($this->_isOneSegment()
            && $this->isValidTable($this->table)
        ) {
            // All attributes present inside the table            
            return ($this->_isValidAttributes());            
        }
        return false;
    }

    /**
     * Populate elements into the input
     *
     * @return void
     */
    public function extractElements()
    {
        $elements = array(
            'table' => $this->table,
            'atts&values' => $_POST,
        );
        $this->input->setElements($elements);
    }


    /**
     * Checks if there's only a single segment which is the only format
     * for a POST request
     *
     * @return Boolean true if only one segement exist else false
     */
    private function _isOneSegment()
    {
        if (count($this->segments) !== 1) {
            $raw = $this->input->getRawInput();
            $method = $this->input->getMethod();
            $this->input->setCodeDes(
                400,
                "{$raw} is an invalid format for a $method Request"
            );
            
            return false;
        }
        $this->table = $this->segments[0];
        return true;        
    }
}
?>