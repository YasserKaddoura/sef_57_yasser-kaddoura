<?php

require_once 'Input.php';
/**
 * Parser parent class
 */
class Parser
{

    protected $segments;
    protected $input;
    protected $table;
    /**
     * Constructor
     *
     * @param Input $input user's input
     */
    protected function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Parses the Input
     *
     * @return void
     */
    protected function parse()
    {
        $this->segments = $this->_getSegments();
    }

    /**
     * Dissects the request into segments
     *
     * @return {Array}
     */
    private function _getSegments()
    {
        return explode('/', rtrim($this->input->getRawInput(), '/'));
    }

    /**
     * Checks if the table is valid
     *
     * @param String $table table name
     *
     * @return Boolean table validation
     */
    protected function isValidTable($table)
    {
        if (!Validator::tableExists($table)) {
            // The table doesn't exist
            $this->input->setCodeDes(
                404,
                "{$table} is an invalid table"
            );
            return false;
        }

        if (!Validator::tableSupported(
            $table,
            $this->input->getMethod()
        )) {
              // The table doesn't exist
            $this->input->setCodeDes(
                405, 
                "USING {$this->input->getMethod()} on $table is not authorized"
            ); 
            return false;
        }
        return true; 
    }

    /**
     * Checks if the last table elements are valid
     *
     * @param String $tables List of tables
     *
     * @return Boolean True if a valid table else False
     */
    protected function isValidTables($tables)
    {
        foreach ($tables as $atts => $value) {
            $tableName = $value['table'];
            if (!$this->isValidTable($tableName)
                || !$this->isValidAttributes($value)
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the attribute is valid
     *
     * @param Array $array attributes list
     *
     * @return Boolean attributes validation
     */
    protected function isValidAttributes($array)
    {
        $tableName = $array['table'];
        $tableElements = $array['elements'];
        if (!isset($tableElements)) {
            return true;
        }
        foreach ($tableElements as $att => $value) {
            if (Validator::attExists($tableName, $att)) {
                // The attribute is valid                        
            } else {
                $this->input->setCodeDes(
                    400,
                    "{$att} is invalid attribute"
                );
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the features are valid
     *
     * @param Array $array Feature list
     *
     * @return Boolean features validation
     */
    protected function isValidFeatures($array)
    {
        $tableElements = $array['elements'];
        if (!isset($tableElements)) {
            return true;
        }
        foreach ($elements as $feature => $value) {
            if (Validator::featureExists($afeature)) {
                // The attribute is valid                        
            } else {
                $this->input->setCodeDes(
                    400,
                    "{$feature} is invalid feature"
                );
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if the features/attributes are valid
     *
     * @param Array $array Attributes/Fetures List
     *
     * @return Boolean features/attributes valid
     */
    protected function isValidAttributesFeatures($array)
    {
        $tableName = $array['table'];
        $tableElements = $array['elements'];
        if (!isset($tableElements)) {
            return true;
        }
        foreach ($tableElements as $att => $value) {
            if (Validator::attExists($tableName, $att)
                || Validator::featureExists($att)
            ) {
                // The attribute is valid                        
            } else {
                $this->input->setCodeDes(
                    400,
                    "{$att} is invalid attribute/feature in table $tableName"
                );
            }       
        }
        return true;
    }
}

?>