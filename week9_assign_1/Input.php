<?php

require_once 'Tools/Validator.php';
/**
 *
 */
class Input
{
    
    // Method used for the request
    private $_method;

    private $_rawInput;
    // Status code
    private $_code;
    // Description about the response
    private $_description;
    // Details about the result query
    private $_result;
    // Array of elements used in CRUD
    private $_elements;

    /**
     * Constructor
     */
    function __construct() 
    {
         // Enabling Cross-Origin Resource Sharing
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        // Set the type of content to send back
        header("Content-Type: application/json");

        $this->_method = $_SERVER['REQUEST_METHOD'];
        if (Validator::methodSupported($this->_method)) {
            $this->_code = 200;
            $this->_rawInput = substr($_SERVER['REQUEST_URI'], 18);   
        } else {
            $this->_code = 405;
            $this->_description = "{$this->_method} is not supported";
        }
    }

    /**
     * Returns the method used with the request
     *
     * @return String - Method used
     */
    public function getMethod()
    {
        return $this->_method;
    }

    /**
     * Returns the rawinput used with the request
     *
     * @return String - Method used
     */
    public function getRawInput()
    {
        return $this->_rawInput;
    }

    /**
     * Returns the status code
     *
     * @return Integer - code used
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * Sets the status code
     *
     * @param integer $code status code
     *
     * @return void
     */
    public function setCode($code)
    {
        $this->_code = $code;
    }

    /**
     * Returns the description
     *
     * @return String - description
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Sets the code and the description
     *
     * @param integer $code        status code
     * @param String  $description description about the response     
     * 
     * @return void
     */
    public function setCodeDes($code, $description)
    {   
        $this->_code = $code;
        $this->_description = $description;
    }

    /**
     * Sets the description
     *
     * @param String $description Description
     *
     * @return void
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * Returns the elements used for CRUD
     *
     * @return array - elements
     */
    public function getElements()
    {
        return $this->_elements;
    }

    /**
     * Sets the elements used for CRUD
     *
     * @param Array $elements Elements for execution
     *
     * @return void
     */
    public function setElements($elements)
    {
        $this->_elements = $elements;
    }

    /**
     * Sets the elements used for CRUD
     *
     * @param String $key   
     * @param String $value      
     *
     * @return void
     */
    public function appendElement($key, $value)
    {
        $this->_elements[$key] = $value;
    }

    /**
     * Returns the result
     *
     * @return Array - Result 
     */
    public function getResult()
    {
        return $this->_result;
    }

    /**
     * Sets the result
     *
     * @param Array $result Result of the query
     *
     * @return void
     */
    public function setResult($result)
    {
        $this->_result = $result;
    }

    /**
     * Returns true if the user needs it pretty orn ot
     *
     * @return {Boolean}
     */
    public function isPretty()
    {
        
        // Get the elemtns of teh last table
        if (!isset($this->_elements[count($this->_elements)-1]['elements'])) {
            return true;
        }

        $lastTable = $this->_elements[count($this->_elements)-1]['elements'];
        if (!isset($lastTable['pretty'])) {
            return true;
        }
        $pretty = $lastTable['pretty'];

        if (strcmp($pretty, 'false') === 0) {
            return false;
        }
        return true;
    }
    /**
     * Returns true if everything is okay
     *
     * @return {Boolean}
     */
    public function isOkay()
    {
        return $this->_code == 200;
    }

    /**
     * Returns true if there's a warning
     *
     * @return {Boolean}
     */
    public function isWarning()
    {
        return $this->_code == 410;
    }

}

?>