/**
 * Game Logic
 */
let game = {
	/**
	 * Game Consturctor
	 * @param {Object} userInput User's input data
	 */
	consutructor: function(userInput) {
		// Information about the game
		// TODO:MAKE IT AS AN OBJECT
		this.generation = 1;
		this.produced = 0;
		this.died = 0;
		this.stayedDead = 0;
		this.stayedAlive = 0;
		this.userInput = userInput;

		this.axis = axis;
		this.gameSize = this.calcGameSize();
		// Number of neighbours for each cell
		this.numNeigh = [[[]]];

		// Range for the conditions
		this.deathRange = [];
		this.produceRange = [];
		// Generate a random or a loaded game
		switch(userInput.generateWay) {
			case generateWay.RANDOM:
				this.map = this.randomGenerator();
				myMode = mode.PLAYING;
				break;
			case generateWay.USER:
				this.map = this.generateEmpty();
				myMode = mode.GENERATING;
				mapStart();
				break;
			case generateWay.LOAD:
				this.map = userInput.gameSelected;
				myMode = mode.PLAYING;
		}
	},
	/**
	 * Set a cell in the map
	 * @param {Integer} x position of the cell in X-axis
	 * @param {Integer} y position of the cell in Y-axis
	 * @param {Integer} z position of the cell in Z-axis
	 */
	setCell: function(x, y, z) {
		this.map[z][y][x] = this.map[z][y][x] === 1 ? 0 : 1;
	},
	/**
	 * Gets the size of the map
	 * @return {Integer} the size of the map
	 */
	calcGameSize: function() {
		return this.userInput.z * this.userInput.y * this.userInput.x;
	},
	/**
	 * Generates a random game
	 * @return {Array} A random generated map of the game
	 */
	randomGenerator: function() {
		let array = [[[]]];
		for (let z = 0; z < this.userInput.z; z++) {
			array[z] = [];
			for (let y = 0; y < this.userInput.y; y++) {
				array[z][y] = [];
				for (let x = 0; x < this.userInput.x; x++) {
					array[z][y][x] = Math.floor(Math.random()*2);
				}
			}
		}
		return array;
	},
	/**
	 * Generates a random game
	 * @return {Array} A random generated map of the game
	 */
	generateEmpty: function() {
		// console.log(userInput);
		let array = [[[]]];
		for (let z = 0; z < this.userInput.z; z++) {
			array[z] = [];
			for (let y = 0; y < this.userInput.y; y++) {
				array[z][y] = [];
				for (let x = 0; x < this.userInput.x; x++) {
					array[z][y][x] = 0;
				}
			}
		}
		return array;
	},
	/**
	 * Next generation for 3D
	 */
	nextGen3D: function() {
		let nextmap = this.map.map(function(arr) {
			return arr.map(function(arr1) {
				return arr1.slice();
			});
		});
		for (let z = 0; z < userInput.z; z++) {
			for (let y = 0; y < userInput.y; y++) {
				for (let x = 0; x < userInput.x; x++) {
					let cell = this.map[z][y][x];
					let numNeigh = this.get3DNeigh(z, y, x);
					let nextGenStatus = this.cellNextGen3D(cell, numNeigh);
					nextmap[z][y][x] = nextGenStatus;
					this.updateStatistics(cell, nextGenStatus);
				}
			}
		}
		this.map = nextmap;
	},
	/**
	 * Returns the number of alive neighbours in 3D
	 * @param {Integer} indexZ Position of a cell in Z axis
	 * @param {Integer} indexY Position of a cell in Y axis
	 * @param {Integer} indexX Position of a cell in X axis
	 * @return {Integer} Number of alive neighbours
	 */
	get3DNeigh: function(indexZ, indexY, indexX) {
		let Alive = 0;
		for (let z = indexZ - 1; z <= indexZ + 1; z++) {
			for (let y = indexY - 1; y <= indexY + 1; y++) {
				for (let x = indexX - 1; x <= indexX + 1; x++) {
					// Skip the cell itself
					if (indexX == x
					&&	indexY == y
					&& 	indexZ == z
					) {
						continue;
					}
					let indexxX;
					let indexyY;
					let indexzZ;
					// If the cell is on the edge
					[indexzZ, indexyY, indexxX] = this.get3DIndex(z, y, x);
					if (this.map[indexzZ][indexyY][indexxX] === 1) {
						Alive++;
					}
				}
			}
		}
		return Alive;
	},
	/**
	 * Returns the state of a certain cell in the next generation
	 * @param {Integer} cell 0-1 dead or alive cell respectively
	 * @param {Integer} aliveNeigh Number of alive neighbors
	 * @return {Integer} The state of the cell in the next gen.
	 */
	cellNextGen3D: function(cell, aliveNeigh) {
		if (cell === 1) {
			if (this.deathRange.includes(aliveNeigh)) {
				return 0;
			} else {
				return 1;
			}
		} else {
			if (this.produceRange.includes(aliveNeigh)) {
				return 1;
			} else {
				return 0;
			}
		}
	},
	/**
	 * Get the index of a cell
	 * @param {Integer} indexZ Index of a cell in x-axis
	 * @param {Integer} indexY Index of a cell in y-axis
	 * @param {Integer} indexX Index of a cell in z-axis
	 * @return {Integer} Number of alive neighbours
	 */
	get3DIndex: function(indexZ, indexY, indexX) {
		if (indexZ === -1) {
			indexZ = this.map.length-1;
		} else if (indexZ === this.map.length) {
			indexZ = 0;
		}
		if (indexY === -1) {
			indexY = this.map[0].length-1;
		} else if (indexY === this.map[0].length) {
			indexY = 0;
		}
		if (indexX === -1) {
			indexX = this.map[0][0].length-1;
		} else if (indexX === this.map[0][0].length) {
			indexX = 0;
		}
		return [indexZ, indexY, indexX];
	},
	/**
	 * Moving the map to the next generation
	 * @param {Integer} Xindex index of a cell in x-axis
	 * @param {Integer} Yindex index of a cell in y-axis
	 */
	nextGen: function() {
		let numRules = rulesUsed.length;
		for (let rule = 0; rule < numRules; rule++) {
			switch (rulesUsed[rule]) {
				case gameRules.thirdD:
				this.nextGen3D();
				break;
			}
		}
		this.generation++;
	},
	/**
	 *  Returns the state of a certain cell in the next generation
	 * @param {Integer} cell 0-1 dead or alive cell respectively
	 * @param {Integer} aliveNeigh Number of alive neighbors
	 * @return {Integer} The state of the cell in the next gen.
	 */
	cellNextGen: function(cell, aliveNeigh) {
		if (cell === 1) {
			if (aliveNeigh < 2 || aliveNeigh > 3) {
				return 0;
			} else {
				return 1;
			}
		} else {
			if (aliveNeigh === 3) {
				return 1;
			} else {
				return 0;
			}
		}
	},
	/**
	 * Updates the next generation statistics
	 * @param {Integer} currStatus current cell status 1-0
	 * @param {Integer} nextStatus next generation cell status 1-0
	 */
	updateStatistics: function(currStatus, nextStatus) {
		if (currStatus === 1) {
			if (nextStatus === 1) {
				this.stayedAlive++;
			} else {
				this.died++;
			}
		} else {
			if (nextStatus === 1) {
				this.produced++;
			} else {
				this.stayedDead++;
			}
		}
	},
	/**
	 * Resets the statiscts
	 */
	resetStatus: function() {
		this.produced = 0;
		this.stayedAlive = 0;
		this.died = 0;
		this.stayedDead = 0;
		this.numNeigh = [[[]]];
	},
};
