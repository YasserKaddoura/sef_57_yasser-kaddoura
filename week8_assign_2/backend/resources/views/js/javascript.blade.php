
<script type="text/javascript">

/**
 * Object that handles the connection with the Websocket server
 * 
 */
var Connection = function() {
    // User's Id
    this.userId= {{ Auth::id() }};
    // User's image name
    this.userImage = '{{ Auth::User()->image_name}}';
    // User's profile name
    this.userName = '{{ Auth::User()->user_name}}';
    this.socket = null;
    //"ws://localhost:12345/chat"
    this.host = "ws://192.168.56.2:9300/chat";
    this.currentChannelId = 1;
    this.action = {
        CONNECT: 1,
        MESS_SEND: 2,
        FILE_SENT: 3,
        CLOSE: 4,
    };
    this.myAction = this.action.CONNECT;
    this.init();
}


/**
 * Create a socket if possible
 *
 * @arg {string} - the host 
 * @return {WebSocket}
 */
Connection.prototype.createSocket = function(host) {
    // If the browser supports sockets
    if ('WebSocket' in window)
        return new WebSocket(host);
    else if ('MozWebSocket' in window)
        return new MozWebSocket(host);

    throw new Error("No web socket support in browser!");
};

/**
 * Instantiate a connection with the WebSocket server
 * and handle all the events
 *
 */
Connection.prototype.init = function() {
    try {
        var myConnection = this;
        this.socket = this.createSocket(this.host);
        //log('WebSocket - status ' + socket.readyState);
        this.socket.onopen = function(msg) {
            myConnection.send(myConnection.myAction);
            //log("Welcome -" + userId + " status " + this.readyState);                        
        };

        // Show the messages sent by the users
        this.socket.onmessage = function(msg) {
            console.log(msg.data);
            var details = JSON.parse(msg.data);
            
            if (details['action'] == myConnection.action.CONNECT || 
                details['action'] == myConnection.action.CLOSE) {
                console.log('connecting or closeing');
                myConnection.changeStatus(details['userId']);
            } else  if (details['action'] == myConnection.action.MESS_SEND) {
                console.log('getting message');
                myConnection.addElement(details);
                
            } else  if (details['action'] == myConnection.action.FILE_SENT) {
                console.log('file');
            }
        };
        
        this.socket.onclose = function(msg) {
            //log("Disconnected - status " + this.readyState);
        };
    }
    catch (ex) {
        //{{-- this.log(ex); --}}
    }
    document.getElementById("msg").focus();
};
/**
 * Change the status of the user (Online-Offline)
 *
 * @arg {integer} - uder id for the person who changed his status
 */
Connection.prototype.changeStatus = function(userId) {
    // The status element in DOM
    var elementId = document.getElementById('status-'+userId);
    // Get the status of the user
    var className = elementId.className;
    // The user went online
    if (className == 'circle-offline' ) {
        elementId.className = 'circle-online';
    } else {
        elementId.className = 'circle-offline';
         
    }
}
/**
 * Add the message to the chat log
 *
 * @arg {Object} - contains the data required for the messsage
 */
Connection.prototype.addElement = function(data) {   

    // The channel id that the message is sent to
    var channelId = "chat-log-" + data['channelId'];
    // Populate a new message using html
    var newMessage = '<div class="row message"> <div class="col s1">' + 
                    '{{ HTML::image("img/profile/original/' + data['image_name'] + 
                    '" ,"alt",array( "width" => 40, "height" => 40, "class" => "responsive-img, circle" )) }}' +
                    '</div><div class="col s11"><div class="row" style="font-size: 1.2wv">' +
                    '<b>' +  data['user_name'] + '</b></div><div class="row">' +
                    '<p style="font-size: 0.6wv">' + data['content'] + '</p> </div></div></div>';
    document.getElementById(channelId).innerHTML += newMessage;
    
};

/**
 * Send a message to the WebSocket server
 *
 */
Connection.prototype.send = function(action) {
    // Details to be sent to the server
    // Filling the details depending on the action
    if (action == this.action.CONNECT ||
        action == this.action.CLOSE) {
        var details = { userId : this.userId, action : action};
    } else if (action == this.action.MESS_SEND) {
        // The content of the message
        var content= document.getElementById('msg').value;
        // Details about the message
        var details = { userId : this.userId, 
                        content : content, 
                        channelId : this.currentChannelId,
                        image_name : this.userImage,
                        user_name : this.userName,
                        action : action
                    };
    } else if (action == this.action.FILE_SENT) {
        var fileObject = document.getElementById('file').files[0];   
        //console.log(file);  
        // get File Object  
// reCreate new Object and set File Data into it
        var newObject  = {
        'lastModified'     : fileObject.lastModified,
        'lastModifiedDate' : fileObject.lastModified,
        'name'             : fileObject.name,
        'size'             : fileObject.size,
        'type'             : fileObject.type
        };  
   
        
        var details = { userId : this.userId, 
                        blob : newObject, 
                        channelId : this.currentChannelId,
                        image_name : this.userImage,
                        user_name : this.userName,
                        action : action
                    };
                      var form_data = new FormData();                  // Creating object of FormData class
    form_data.append("file", fileObject);            // Appending parameter named file with properties of file_field to form_data
    form_data.append("user_id", 123);            // Adding extra parameters to form_data
        console.log(fileObject);
                    
    }

    // Changing the object to a string to send it to the webSocket server
    var msg = JSON.stringify(details);
    try {
        this.socket.send(msg);
    } catch (ex) {
        console.log(ex);
    }
   
};
function a() {
     // Changing the object to a string to send it to the webSocket server
    var msg = JSON.stringify(details);
            
    try {
        this.socket.send(msg);
    } catch (ex) {
        console.log(ex);
    }
}
/**
 * Cloes the connection with the WebSocket server
 *
 */
Connection.prototype.quit = function() {
    log("Goodbye!");
    socket.close();
    socket = null;
};

/**
 * Print the message on the channel
 *
 */
Connection.prototype.log = function(msg) {
    var channelId = "chat-log-" + msg['channelId'];
    document.getElementById(channelId).innerHTML += "<br>" + msg['content'];
};


window.onload = function () {

    var myConnection = new Connection();
    var channels = document.getElementsByClassName("channel");
    // Add an addEventListener to each channel string
    Array.from(channels).forEach(function(element) {
            element.addEventListener('click', changeChannel);
  
    });
    function changeChannel() {
        // If the channel clicked isn't the current channel
        if (myConnection.currentChannelId != this.id) {
            document.getElementById('chat-log-' + myConnection.currentChannelId).style.visibility = "hidden";
            console.log(this.id);
            document.getElementById('chat-log-' + this.id).style.visibility = "visible";
            myConnection.currentChannelId = this.id
            
        }
    }
        /**
    * GLOBAL VARIABLES
    */
    var inputBox = document.getElementById("msg");
    var file = document.getElementById("file");
    
    
    /**
    * EVENT HANDLERS
    */ 

    /**
    * The Enter button calls the send method
    */
    inputBox.addEventListener('keydown', function (event) {
        if (event.keyCode == 13) {
            myConnection.send(myConnection.action.MESS_SEND);
        }
    });
        
    file.addEventListener('change', function (event) {
        myConnection.send(myConnection.action.FILE_SENT);
    });
};

</script>