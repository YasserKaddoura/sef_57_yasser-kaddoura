
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Chat') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <!-- Materialize -->
    <link href="{{ asset('css/materialize-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <!-- My Style -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery-3.2.0.min.js') }}"></script>
    <script src="{{ asset('js/materialize.js') }}"></script>
    <script src="{{ asset('js/init.js') }}"></script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
         
        console.log(':)');
        $(".dropdown-button").dropdown();
    
    </script>
</head>
<body>

        
    <div id="app">

    <!-- Dropdown Structure -->
        
            <nav>
                <div class="nav-wrapper green">
                    <a href="javascript:;" class="brand-logo center">{{ config('app.name', 'Chat') }}</a>
                    @if (!Auth::guest())
                        <ul class="right">
                            <!-- Dropdown Trigger -->
                            <li><a class="dropdown-button" href="#!" data-activates="dropdown1">{{ Auth::user()->user_name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                        </ul>
                    @endif
                </div>
            </nav>
        <ul id="dropdown1" class="dropdown-content">
            @if ((!Auth::guest()) && Auth::user()->is_admin)
                <li><a href="{{ url('server/edit') }}">Manage Server</a></li>
            @endif
            <li class="divider"></li>
            <li>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
 
        @yield('content')
    </div>

    <!-- Scripts -->
</body>
</html>