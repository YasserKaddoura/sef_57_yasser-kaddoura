@extends('layouts.master')

@section('title')
    Admin
@endsection

@section('content')
<!-- New Channel form -->
<form class="form-horizontal" role="form" method="POST" action="{{ url('channel') }}">
    {{ csrf_field() }}

    
    <div class="row">
        <div class="row">
            <div class="input-field col s12">
                <input type="text" id="name" class="materialize-textarea" name="name" class="validate">
                <label for="name">Channel Name</label>
            </div>
        </div>
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div class="row center">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="waves-effect waves-light btn">
                Create Channel
            </button>
        </div>
    </div>
</form>

<!-- User admin form-->
<form class="form-horizontal" role="form" method="POST" action="{{ url('role') }}">
    {{ csrf_field() }}
    @foreach ($users as $user)
            <div class="row center">
                <div class="col-md-6 col-md-offset-4">
                    @if ($user->is_admin)
                        <input type="checkbox" class="filled-in" id="checkbox{{ $user->id }}" checked="checked" name="checkbox{{ $user->id }}"/>  
                    @else
                        <input type="checkbox" class="filled-in" id="checkbox{{ $user->id }}" name="checkbox{{ $user->id }}"/>            
                    @endif
                        <label for="checkbox{{ $user->id }}">{{ $user->user_name }}</label>
                </div>
            </div>
    @endforeach
    <div class="row center">
        <div class="col-md-6 col-md-offset-4">
    {{-- <input name="_method" type="hidden" value="PUT"> --}}
        
            <button type="submit" class="waves-effect waves-light btn">
                Change statuses
            </button>
        </div>
    </div>
</form>
@endsection