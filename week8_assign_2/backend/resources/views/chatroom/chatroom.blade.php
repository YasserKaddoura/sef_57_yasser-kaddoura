@extends('layouts.master')

@section('title')

    Chatroom

@endsection

@section('content')

    <!-- Start of container -->
    <div class="container chatroom" >
        <div class ="row card-panel brown chatroom-row">
            <!-- Start of side navbar -->
            <div class="col s2 m2 l2 card-panel  green darken-2 black-text text-darken-2text-darken-2 server valign-wrapper">
                <!-- The header -->
                <div class="row nav-header valign">
                            <b class="center-align" id="server-name">
                            SE Factory
                            </b>
                        <div class="row element valign-wrapper">
                            <div class="col s1 status ">
                                </span>
                            </div>
                            <div class="col s11 name" >
                                {{ Auth::user()->user_name }}
                            </div>
                        </div>
                </div>
                <!-- Channels -->
                <div class="row channels" >
                     <b>
                        Channels({{ count($channels) }})
                    </b>
                    @foreach ($channels as $channel)
                        <div class="row element valign-wrapper channel" id="{{ $channel->id}}">
                                <div class="col s1 status ">
                                    #
                                </div>
                                <div class="col s11 name" >
                                    {{ $channel->name }}
                                </div>
                        </div>
                    @endforeach
                </div>
                <!-- Users -->
                <div class="row nav-header">
                    <b>
                    Users
                    </b>
                    @foreach ($users as $user)
                        <div class="row element valign-wrapper">
                            <div class="col s2 m2 status ">
                                <span class="circle-offline" id="status-{{ $user->id }}">
                                </span>
                            </div>
                            <div class="col s10 m10 name" >
                                {{ $user->user_name }}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- End of side navbar -->
            <!-- Start of the chatlog -->
            <div class="col s10 m10 l10 card-panel chat-section">
                <div class="row" style="height: 90%;">
                <!-- Get each channel chat-log -->
                    @foreach($channels as $channel)
                        <div class="chat-log" id="chat-log-{{ $channel->id }}">
                            <!-- Messages -->
                            @foreach($channel->messages as $message)
                                <div class="row message">
                                    <div class="col s1">
                                        {{ HTML::image('img/profile/original/' . $message->user->image_name, 'alt', array( 'width' => 40, 'height' => 40, 'class' => 'responsive-img, circle' )) }}
                                    </div>
                                    <div class="col s11">
                                        <div class="row" style="font-size: 1.2wv">
                                            <b class="col "> {{ $message->user->user_name }}</b>
                                            <b class="col offset-s1"> {{ $message->created_at->format('D H:s') }}</b>
                                            
                                        </div>
                                        <div class="row">
                                            <p style="font-size: 0.6wv">{{$message->content}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                
            </div>
            <!-- User Input -->
                <div class="row" style="position: relative;">
                    <input id="msg" type="text"/>
                </div>
                <div class="row" style="position: relative;">
                    <input type="file" id="file" />
                </div>
            <!-- End of the chatlog -->
            
        </div>
    </div>
    <!-- End of container -->
@include('js.javascript')

@endsection