<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            // Primary key
            $table->increments('id');
            // User who sent the message
            $table->integer('user_id')->unsigned();
            // The channel that the message was sent to
            $table->integer('channel_id')->unsigned();
            // Content of the message
            $table->text('content');
            $table->timestamps();

            // Indexing
            // Foreign key for the user table
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            // Foreign key for the channel table
            $table->foreign('channel_id')
                  ->references('id')->on('channels')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_lines');
    }
}
