<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            // The primary key
            $table->increments('id');
            // User profile name
            $table->string('user_name')->unique();
            // User's email
            $table->string('email')->unique();
            // The profile picture name of the user
            $table->string('image_name')->unique();
            // User's password         
            $table->string('password');
            // Determines if the user is admin
            $table->boolean('is_admin')->default(false);            
            // Used for session tracking
            $table->rememberToken();
            // Tracking the insertions and updates of the record
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
