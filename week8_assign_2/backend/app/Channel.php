<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

     /**
      * One - Many relationship with message
      *
      * Returns the messages belongs to the channel
      */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
