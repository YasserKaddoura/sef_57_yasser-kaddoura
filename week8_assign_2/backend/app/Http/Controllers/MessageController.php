<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $request;
        //  // Instantiate a message
        // $chatLog = Message::create([
        //     'user_id' => (int)request('user_id'),
        //     'channel_id' => (int)request('channel_id'),
        //     'content' => request('content')
        // ]);
        // // Store the message in the database
        // $chatLog->save;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChatLine  $chatLine
     * @return \Illuminate\Http\Response
     */
    public function show(ChatLine $chatLine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChatLine  $chatLine
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatLine $chatLine)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChatLine  $chatLine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatLine $chatLine)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChatLine  $chatLine
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatLine $chatLine)
    {
        //
    }
}
