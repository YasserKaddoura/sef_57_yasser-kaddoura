<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Channel;
use App\User;

class ServerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a the server
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // Get all the channels
        $channels = Channel::all();
        $users = User::all();
        // Return the view with all the available channels
        return view('chatroom.chatroom', compact('channels', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the admin page
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // If user is not an admin don't let him enter
        if (!Auth::user()->is_admin) {
            abort(403, 'Unauthorized action.');
        }
        // Fetch all the users
        $users = User::all();
        // Display the view
        return view('chatroom.edit', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
