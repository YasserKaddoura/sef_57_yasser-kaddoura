<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


use Image;
use File;
use Storage;

class RegisterController extends Controller
{

    use RegistersUsers;
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'server';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

         // The paths for the image 
        $thumbnail_path = public_path('img/profile/thumbnail/');
        $original_path = public_path('img/profile/original/');

        
        // Make the directories  with privileges 
        // if they doesn't exist
        if (!is_dir($thumbnail_path)) {
            mkdir($thumbnail_path, 0775, true);
        }
        if (!is_dir($original_path)) {
            mkdir($original_path, 0775, true);
        }

        // Generating a timestap to be used for the profile picture
        $date = date_create();
        $timeStamp = date_timestamp_get($date);

        if (isset($data['image'])) {
            // Get the image
            $file = $data['image'];
            // Get the extension
            $file_ext = $file->getClientOriginalExtension();
            
        } else {
            // Set a default image
            $file = File::get(public_path('img/profile/default.png'));
            $file_ext = 'png';

        }
         // Image name 
        $file_name = $timeStamp . '.' . $file_ext;

        // Instantiate a user model
        $user = User::create([
            'user_name' => $data['user_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'image_name' => $file_name,
            
        ]);

        // Resizing the image and storing it
        Image::make($file)
        ->resize(261,null,function ($constraint) {
        $constraint->aspectRatio();
            })
        ->save($original_path . $file_name)
        ->resize(90, 90)
        ->save($thumbnail_path . $file_name);

        return $user;
    }
}
