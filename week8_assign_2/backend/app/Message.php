<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'user_id', 'channel_id', 'content'
    ];
    //  protected $dateFormat = 'Y';
    /**
    * One - Many relationship with channel
    *
    * Returns the channel that the message belongs to
    */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    /**
    * One - Many relationship with user
    *
    * Returns the user who made the message
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
