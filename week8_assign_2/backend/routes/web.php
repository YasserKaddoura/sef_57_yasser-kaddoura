<?php

Route::get('/', function () {
    return redirect('server');
});
// Handles the authentication
Auth::routes();

// Handles the insertion of messages
Route::resource('message', 'MessageController');
// Handles the addition of channels
Route::resource('channel', 'ChannelController');
// Handles the display of the server
Route::resource('server', 'ServerController');
// Handles the roles of the users
Route::resource('role', 'RoleController');


?>