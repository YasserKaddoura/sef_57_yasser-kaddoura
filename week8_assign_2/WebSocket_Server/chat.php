#!/php -q
<?php


require_once("/var/www/html/week8_assign_2/WebSocket_Server/vendor/autoload.php");


// Set timezone of script to UTC inorder to avoid DateTime warnings in
// vendor/zendframework/zend-log/Zend/Log/Logger.php
date_default_timezone_set('UTC');


// Run from command prompt > php chat.php
use Devristo\Phpws\Framing\WebSocketFrame;
use Devristo\Phpws\Framing\WebSocketOpcode;
use Devristo\Phpws\Messaging\WebSocketMessageInterface;
use Devristo\Phpws\Protocol\WebSocketTransportInterface;
use Devristo\Phpws\Server\IWebSocketServerObserver;
use Devristo\Phpws\Server\UriHandler\WebSocketUriHandler;
use Devristo\Phpws\Server\WebSocketServer;

/**
 * This ChatHandler handler below will respond to all messages sent to /chat (e.g. ws://localhost:12345/chat)
 */
class ChatHandler extends WebSocketUriHandler {

    /**
     * Notify everyone when a user has joined the chat
     *
     * @param WebSocketTransportInterface $user
     */
    public function onConnect(WebSocketTransportInterface $user){
        // Get each client using the connection
        foreach($this->getConnections() as $client){
            // Notify that a user has joined
            // $client->sendString($user->getId());
        }
    }

    /**
     * Broadcast messages sent by a user to everyone in the room
     *
     * @param WebSocketTransportInterface $user
     * @param WebSocketMessageInterface $msg
     */
    public function onMessage(WebSocketTransportInterface $user, WebSocketMessageInterface $msg) {
        // Decode the message
        $contents = json_decode($msg->getData(), True);
        $this->logger->notice("Broadcasting " . $contents['userId'] . " bytes");
        // Send the message details to each subscriber
        foreach($this->getConnections() as $client){
            $client->sendString($msg->getData());
        }
        if($contents['action'] == 2){
            // Store it in the datbase
            $this->storeIndatabase (array("user_id" => $contents['userId'], 
                                        "channel_id" => $contents['channelId'],
                                        "content" => $contents['content'])); 
        }  elseif ( $contents['action'] == 1 ) {
            var_dump('connecting');
        }
        elseif ( $contents['action'] == 3 ) {
        $this->storeIndatabase ($contents); 
            
        }  elseif ( $contents['action'] == 4 ) {
            var_dump('closing');
        }
    }
    /**
     * Stores the message of the user in the databse
     *
     * @arg {array} - array containing details of the message
     */
    function storeIndatabase ($content) {
        $ch = curl_init('localhost/week8_assign_2/backend/public/message');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);  // RETURN THE CONTENTS OF THE CALL
        $resp = curl_exec($ch);
        var_dump($resp);
        curl_close($ch);
    }
}
class ChatHandlerForUnroutedUrls extends WebSocketUriHandler {
    /**
     * This class deals with users who are not routed
     */
    public function onConnect(WebSocketTransportInterface $user){
		//do nothing
		$this->logger->notice("User {$user->getId()} did not join any room");
    }
    public function onMessage(WebSocketTransportInterface $user, WebSocketMessageInterface $msg) {
    	//do nothing
        $this->logger->notice("User {$user->getId()} is not in a room but tried to say: {$msg->getData()}");
    }

}



$loop = \React\EventLoop\Factory::create();

// Create a logger which writes everything to the STDOUT
$logger = new \Zend\Log\Logger();
$writer = new Zend\Log\Writer\Stream("php://output");
$logger->addWriter($writer);

// Create a WebSocket server
$server = new WebSocketServer("tcp://192.168.56.2:9300", $loop, $logger);

// Create a router which transfers all /chat connections to the ChatHandler class
$router = new \Devristo\Phpws\Server\UriHandler\ClientRouter($server, $logger);
// route /chat url
$router->addRoute('#^/chat$#i', new ChatHandler($logger));
// route unmatched urls durring this demo to avoid errors
$router->addRoute('#^(.*)$#i', new ChatHandlerForUnroutedUrls($logger));

// Bind the server
$server->bind();

// Start the event loop
$loop->run();

?>
