/* Testing commands*/
select * from FiscalYearTable;
delete from FiscalYearTable;
drop trigger ROWVALIDATION;
SELECT FT.fiscal_year FROM `FinanceDB`.`FiscalYearTable` as FT WHERE '1999-05-07' BETWEEN FT.start_date AND FT.end_date;

insert into FiscalYearTable values (null,'1994','1999-4-1','1999-4-1');

/* Table creation*/
CREATE table FiscalYearTable (ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
                              fiscal_year YEAR NOT NULL UNIQUE,
                              start_date DATE NOT NULL ,
                              end_date DATE NOT NULL);
/* Notes:
* - vague : fiscal_year contains unique values?
                            
/* Assigning a new DELIMITER*/
DELIMITER $$
CREATE TRIGGER ROWVALIDATION 
    BEFORE INSERT ON FiscalYearTable 
    FOR EACH ROW
    BEGIN
         /* The fiscal_year input should be equal to the year of the end_date*/
         IF (YEAR(NEW.end_date) != NEW.fiscal_year)
            THEN
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'Error : The fiscal_year should be the same as the year of the end_date';
         END IF;
         /* The start date should have 1 as a day*/
         IF (DAY(NEW.start_date) != 1)
            THEN
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'Error : The start day for a start date should be 1';
         END IF;
         /* The end date should be a (1 year- 1 day) difference between the start_date*/
         IF (date_add(date_sub(NEW.start_date, interval 1 day), interval 1 year) != NEW.end_date)
            THEN
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'Error : Invalid end date';    
         END IF;
    END;
$$
/* Restting a new DELIMITER*/
DELIMITER ;
