-select b.proc_id, count(*) from Procedures a left join Procedures b on a.anest_name = b.anest_name group by b.proc_id;

-select b.proc_id, count(*) from Procedures a left join Procedures b  on a.anest_name = b.anest_name group by case when a.start_time < b.end_time and a.end_time > b.start_time then 1 else 0 end, proc_id;

-select a.proc_id, sum(case when a.start_time  < b.end_time and a.end_time > b.start_time then 1 else 0 end) as p from Procedures a inner join Procedures b  on a.anest_name = b.anest_name group by proc_id;
-select a.proc_id, count(*) as p from Procedures a inner join Procedures b  on a.anest_name = b.anest_name where a.start_time  < b.end_time and a.end_time > b.start_time group by proc_id;

-select * from (select a.proc_id as id,b.start_time as astart,b.end_time as aend, case when a.start_time  < b.end_time 
and a.end_time > b.start_time then 1 else 0 end as count from Procedures a right join Procedures b  
on a.anest_name = b.anest_name) as test1 left join (select a.proc_id as id,b.start_time as bstart, b.end_time as bend, 
case when a.start_time  < b.end_time and a.end_time > b.start_time then 1 else 0 end as 
count from Procedures a right join Procedures b  on a.anest_name = b.anest_name) as test2 on test1.id = test2.id where test1.astart  < test2.bend and test1.aend  > test2.bstart;


-select test1.id,test2.id,count(*) as count
 from (select a.proc_id as id,b.start_time as astart,b.end_time as aend from Procedures a inner join Procedures b  
on a.anest_name = b.anest_name where a.start_time  < b.end_time and a.end_time  > b.start_time) as test1 inner join 
(select a.proc_id as id,b.start_time as bstart, b.end_time as bend 
from Procedures a inner join Procedures b  on a.anest_name = b.anest_name where a.start_time  < b.end_time and a.end_time  > b.start_time) 
as test2 on test1.id = test2.id where test1.astart  => test2.bstart and test1.astart  < test2.bend
group by test1.id,test2.id;   
/*******************************************************/

CREATE TABLE AnestProcedures(
proc_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
anest_name VARCHAR(100) NOT NULL,
start_time TIME NOT NULL,
end_time TIME NOT NULL);

INSERT INTO AnestProcedures VALUES (NULL, 'Albert', '08:00', '11:00');
INSERT INTO AnestProcedures VALUES (NULL, 'Albert', '09:00', '13:00');
INSERT INTO AnestProcedures VALUES (NULL, 'Kamal', '08:00', '13:30');
INSERT INTO AnestProcedures VALUES (NULL, 'Kamal', '09:00', '15:30');
INSERT INTO AnestProcedures VALUES (NULL, 'Kamal', '10:00', '11:30');
INSERT INTO AnestProcedures VALUES (NULL, 'Kamal', '12:30', '13:30');
INSERT INTO AnestProcedures VALUES (NULL, 'Kamal', '13:30', '14:30');
INSERT INTO AnestProcedures VALUES (NULL, 'Kamal', '18:30', '19:00');
 

SELECT P1.proc_id AS id, P2.proc_id, P3.proc_id
FROM AnestProcedures AS P1, AnestProcedures AS P2, AnestProcedures AS P3
WHERE P1.anest_name = P2.anest_name AND P1.anest_name = P3.anest_name
AND P1.start_time <= P2.start_time AND P1.end_time > P2.start_time
AND P3.start_time <= P2.start_time AND P3.end_time > P2.start_time

SELECT id AS proc_id, MAX(total) AS max_inst_count
FROM (SELECT P1.proc_id AS id, P2.proc_id, COUNT(*) AS total
FROM AnestProcedures AS P1, AnestProcedures AS P2, AnestProcedures AS P3
WHERE P1.anest_name = P2.anest_name AND P1.anest_name = P3.anest_name
AND P1.start_time <= P2.start_time AND P1.end_time > P2.start_time
AND P3.start_time <= P2.start_time AND P3.end_time > P2.start_time
GROUP BY P1.proc_id, P2.proc_id) AS temp GROUP BY id;
/***********************************************/