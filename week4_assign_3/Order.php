<?php
session_save_path('/tmp');
session_start();

require_once "Config.php";
require_once "MySQLWrap.php";
require_once "Customer.php";

// Check if the page is refreshed
$pageWasRefreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';

// If page is refreshes unset $_POST
if($pageWasRefreshed ) {
    $_POST = NULL;
    unset($_POST);
}

// If there's no session redirect to the log in page
if(empty($_SESSION)){

    header('Location:Login.php');
    exit();

} else {
    
    // Get the customer object from the session
    $customer = unserialize($_SESSION['customer']);
    $storeId = $customer -> getStoreId();

    $db = new MySQLWrap();
    $db -> connect ();
    // Result of the query that get all available movies  
    $result = $db -> select ("  SELECT distinct `title`
                                FROM 
                                (SELECT 
                                inv.film_id
                                FROM
                                inventory AS inv
                                WHERE
                                inventory_id NOT IN (SELECT /* Current rented movies */
                                rented.inventory_id
                                FROM
                                rental AS rented 
                                WHERE
                                rented.return_date IS NULL)
                                AND inv.store_id = {$storeId}) as filmsAvail JOIN /* To get the film titles*/
                                film ON filmsAvail.film_id = film.film_id
                                ");
    $db -> disconnect ();
    // Problem from the backend
    if ($result === FALSE) {
        echo "CALL THE DEVS";
    }
    
    if (!empty($result)){

     $movies = $result;
     
     } else {

        echo "No movies for renting";
    }

}
?>
<!DOCTYPE html>
<html>

<head>
    <link 
        rel = 'stylesheet'
        type =  'text/css'
        href = 'default.css' />
    <title>PICK ONE MOVIE</title>

</head>
<body >
    <form action = 'OrderProcess.php' method = 'post'>
        <fieldset>
            <legend>MOVIE RENTING:</legend>
            Movies:<br>
            <input list = 'movies' name =' movie'>
            <datalist id = 'movies'>
                <?php 
                if ($movies != NULL){
                    foreach ($movies as $movie) {
                        echo '<option value = "' . $movie["title"] . '">' ;
                    }
                }
                ?>
            </datalist><br><br>
            <input type = 'submit' value = 'Submit'>
        </fieldset>
    </form>
</body>
</html>
