<?php
session_save_path('/tmp');
session_start();

require_once "Config.php";
require_once "MySQLWrap.php";
require_once "Customer.php";

// Check if the page is refreshed
$pageWasRefreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';

// If page is refreshed go back to Order.php
if($pageWasRefreshed ) {
    header('Location:Order.php');
    exit();        

    // $_POST = NULL;
    // unset($_POST);
}

// Check user input
if (isset($_POST['movie']) && !empty($_POST['movie'])) {

    // Store the movie in a variable
    $movie = $_POST['movie'];

    // Get the customer from session
    $customer = unserialize($_SESSION['customer']);

    // Get the store id to check its inventory
    $storeId = $customer -> getStoreId();

    // Get customer id to insert in rental
    $customerId = $customer -> getCustomerId();

    // Connect to the database
    $db = new MySQLWrap();

    // Check if the title is available
    $db -> connect();
    $result = $db -> select("SELECT 
                            *
                            FROM
                            inventory AS inv
                            JOIN
                            film ON film.film_id = inv.film_id
                            AND inv.store_id = $storeId
                            AND film.title = '{$movie}'");
    $db -> disconnect();

    // Problem from the backend
    if ($result === FALSE) {

        echo "CALL THE DEVS - MOV-AVAI";

    } elseif (empty($result)){

        // Movie not available 
        echo "We don't sell {$movie}";

    } else {

        // Check movie if in stock
        $db -> connect();
        $result = $db -> select ("  SELECT inventory_id
                                    FROM 
                                    (SELECT 
                                    inv.inventory_id, inv.film_id
                                    FROM
                                    inventory AS inv
                                    WHERE
                                    inventory_id NOT IN (SELECT /* Current rented movies */
                                    rent.inventory_id
                                    FROM
                                    rental AS rent
                                    WHERE
                                    rent.return_date IS NULL)
                                    AND inv.store_id = {$storeId}) as filmsAvail JOIN /* To get the film titles*/
                                    film ON filmsAvail.film_id = film.film_id WHERE title = '$movie' LIMIT 1");
        $db -> disconnect();

        if ($result === FALSE) {

            // Error backend
            echo "CALL THE DEVS - CODE:MOV-STOCK";
            
        } elseif (empty($result)) {
        
            // The movie is out of stock
            echo "Better luck next time. Out of stock!";

        } else {
            
            // Disable autocommiting to attain atomicity
            $db -> connect ();
            $db -> transaction();
            
            // Get the available inventory id from result
            $inventoryId = (int)$result[0]["inventory_id"];
            
            // Record the transaction in rental
            $result = $db -> insert ("INSERT INTO rental (`customer_id`,                                      `inventory_id`, `staff_id`) 
                                    VALUES ($customerId, $inventoryId, 1)");

            // Backend error
            if ($result === FALSE) {
                echo "CALL THE DEVS - CODE:REN-INS";
            } else {
                // Record the transaction in payment
                $result = $db -> insert ("INSERT INTO payment (`customer_id`,                             `staff_id`, `rental_id`, `amount`) 
                                          VALUES ($customerId, 1, $result, 5)");

                // Problem from backend
                if ($result == FALSE) {
                    echo "CALL THE DEVS - CODE:PAY-INS";
                } else {

                    // Commit the rent transaction
                    $db -> commit();
                    $db -> disconnect();
                    // Successful transaction
                    echo "Your movie will arrive soon!";
                } 
            }
        }
    }
} else {

    // Redirect to order page on empty submition
    header('Location:Order.php'); 
}

?>