<?php
require_once 'Config.php';

Class MySQLWrap
{ 
    // The database connection
    private $_connection;
    
    /**
     * Constructor
     */
    public function __construct()
    {   
    }
    
    /**
     * Connect to the database
     *
     * @return bool FALSE on failure / mysqli MySQLi object instance on success
     */
    public function connect()
    {
        // Connect if not connected already
        if (!isset($this->_connection) || !$this->_connection->ping()) {
            $this->_connection = new mysqli(HOST, USER, PASSWORD, DATABASE);   
        }

        // If connection failed, alarm the user
        if ($this->_connection === FALSE) {
            echo "The mistake is from our side!";
        }
    }

    /**
     * Set the database connection autocommit to false to attain atomicity
     */
    public function transaction()
    {
        $this -> _connection -> autocommit(FALSE);
    }

    /**
     * Commit transaction
     */
    public function commit()
    {
        if (!$this -> _connection -> commit()) {
            echo 'CALL THE DEVS-CODE:TRANS-FAIL';        
        }
    }
    
    /**
     * Disconnect the database connection
     */
    public function disconnect()
    {
        $this -> _connection -> close();
    }

    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param $query The query string
     * @return bool False on failure / array Database rows on success
     */
    public function query($query)
    {
        // Connect to the database
        $this -> connect();

        // Query the database
        $result = $this -> _connection -> query($query);

        return $result;
    }

    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param $query The query string
     * @return bool False on failure / array Database rows on success
     */
    public function select($query)
    {
        $rows = array();
        $result = $this -> query($query);
        if ($result === FALSE) {
            return FALSE;
        }
        while ($row = $result -> fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Insert a row into the database (SELECT query)
     *
     * @param $query The query string
     * @return bool False on failure / integer of the inserted id on success
     */
    public function insert($query)
    {
        $result = $this -> query($query);               

        if ($result === FALSE) {
            return FALSE;
        }
        $idInserted = $this -> _connection -> insert_id;

        return $idInserted;
    }  
}


?>