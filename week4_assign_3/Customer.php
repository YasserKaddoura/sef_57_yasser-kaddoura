<?php
Class Customer {

    // Infromation about the customer
    private $_firstName;
    private $_lastName;
    private $_storeId;
    private $_email;
    private $_customerId;

    /**
    * Constructor
    */
    public function __construct($firstName, $lastName, $email,                                              $storeId, $customerId)
    {   
        $this -> _firstName = $firstName;
        $this -> _lastName = $lastName;
        $this -> _storeId = $storeId;
        $this -> _email = $email;        
        $this -> _customerId = $customerId;
    }

    /**
    * @return string first name 
    */
    public function getFirstName()
    {
        return $this -> _firstName;
    }

    /**
    * @return string last name
    */
    public function getLastName()
    {
        return $this -> _lastName;
    }

    /**
    * @return integer store id
    */
    public function getStoreId()
    {
        return $this -> _storeId;
    }

    /**
    * @return string email 
    */
    public function getEmail()
    {
        return $this -> _email;
    }

    /**
    * @return integer customer id 
    */
    public function getCustomerId()
    {
        return $this -> _customerId;
    }
}
?>