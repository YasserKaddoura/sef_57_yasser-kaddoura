<?php 
session_save_path('/tmp');
session_start();

require_once "Customer.php";
require "MySQLWrap.php";

// Check if the page is refreshed
$pageWasRefreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';

// If page is refreshes unset $_POST
if ($pageWasRefreshed ) {
    $_POST = NULL;
    unset($_POST);
}

// Check Input
if (isset($_POST['email']) && !empty($_POST['email'])) {
    // Connect to database
    $db = new MySQLWrap();
    
    // Assign the email to a variable
    $userEmail = $_POST['email'];

    // Validate the email
    $db -> connect();
    $result = $db -> select("SELECT * FROM `customer` 
                             WHERE `email` = '{$userEmail}'");    
    $db -> disconnect();

    // Problem from the backend
    if ($result === FALSE) {
        echo 'ALL THE DEVS - CODE:SELE-EM';
    }

    // Instantiate a customer on success
    if (!empty($result)) {

        // Get the customer info from result
        $customerInfo = $result[0];

        // Instantiate a customer object
        $db -> connect();
        $customer = new Customer($customerInfo['first_name'],
                                 $customerInfo['last_name'],
                                 $userEmail,
                                 (int)$customerInfo['store_id'],
                                 (int)$customerInfo['customer_id']);
        $db -> disconnect();
        
        // Preserve the customer object using session
        $_SESSION['customer'] = serialize($customer);

        // Redirect the user to Order.php
        header('Location:Order.php');        
        exit();

    } else {

        // Invalid email
        echo "Invalid customer";

    }  
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>LOGIN, SORT OF ?!</title>
    <link 
        rel = 'stylesheet'
        type =  'text/css'
        href = 'default.css' />
</head>
<body>
    <form method = 'POST'>
        <fieldset>
            <legend>Customer validation:</legend>
            Email:<br>
            <input type = 'email' name = 'email' value = ''><br>
            <input type = 'submit' value = 'Submit'>
        </fieldset>
    </form>
</body>
</html>
