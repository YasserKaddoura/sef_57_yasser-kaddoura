#!/usr/bin/php
<?php

# SE-Factory Week1_Assign2_Ex1
# Author: Yasser Kaddoura
# Version: 0.2

// TODO: Make it possible to get more arguments
$optionsPresent = false;
define('OPTION', '-i');

/**
 * Checks if the user input is valid and returns the input
 * @param array $args command prompt input
 * @return bool/string
 */
function parse($args)
{
    if (count($args) === 3
        && strcmp($args[1], OPTION) === 0
    ) {
        return $args[2];
    }
    return false;
}

/**
 * Checks for valid directory
 * @param string $path path to a directory
 * @return bool
 */
function isValidDir($path) {
    return is_dir ( $path);
}

/**
 * Checks for valid file
 * @param string $path path to a file
 * @return bool
 */
function isValidFile($path) {
    return is_file ( $path );
}

/**
 * A recursive function that gets all the files related to a path
 * @param string $directory Path of a directory
 */
function getFiles($directory) {
    // Get everything inside of a path
    $files = scandir($directory);
    foreach ($files as $file) {
        if(strcmp($file, '.') !== 0
            && strcmp($file, '..') !== 0) {
            $fileDirectory = $directory.'/'.$file;
            if (isValidFile($fileDirectory)) {
                echo $file . PHP_EOL;
            } elseif (isValidDir($fileDirectory) === true) {
                getFiles($directory.'/'.$file);
            }
        }
    }
    return;
}

/**
 * The main function of the script
 * @param array $argv command prompt input
 */
function main($argv) {
    if ($directory = parse($argv)) {
        if (isValidDir($directory)) {
            getFiles($directory);
        } else {
            echo $directory . ' is an invalid directory!' . PHP_EOL;
        }
    } else {
        echo 'Error: command i not found!' . PHP_EOL;
    }
}
main($argv);
?>
