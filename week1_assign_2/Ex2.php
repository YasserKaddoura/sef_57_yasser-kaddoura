#!/usr/bin/php  
<?php

# SE-Factory Week1_Assign2_Ex2
# Author: Yasser Kaddoura
# Version: 0.2

class Parser {

    private $_log;

    function __construct($log) {
        $this->_log = $log;
    }

    /**
     * @return string the IP address
     */
    public function getIP() {
        $matches = array();
        preg_match('/^\d*.\d*.\d*.\d*/', $this->_log, $matches);
        return $matches[0];
    }

    /**
     * @return string the date in the desired format
     */
    public function getDate() {
        $matches = array();
        preg_match('/\[(.*)\]/', $this->_log, $matches);
        $dt = new DateTime($matches[1]);
        $date = $dt->format('l, F Y : H-i-s');
        return $date;
    }

    /**
     * @return string the Header
     */
    public function getHeader() {
        $matches = array();
        preg_match('/".*?"/', $this->_log, $matches);
        return $matches[0];
    }

    /**
     * @return string the request code
     */
    public function getCode() {
        $matches = array();
        preg_match('/"\s(\d*)/', $this->_log, $matches);
        return $matches[1];
    }
}

function main() {

    $rawContent = file_get_contents('/var/log/apache2/access.log', FILE_USE_INCLUDE_PATH);
    $listLog = explode(PHP_EOL, $rawContent);
    array_pop($listLog);

    foreach ($listLog as $value) {
        $myParser = new Parser($value);
        echo $myParser->getIP().' -- '.
             $myParser->getDate().' -- '.
             $myParser->getHeader().' -- '.
             $myParser->getCode() . PHP_EOL;
    }
}

main();
?>
