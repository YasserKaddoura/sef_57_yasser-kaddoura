USE sakila;

SELECT 
    CONCAT(A.first_name, ' ', A.last_name) AS `Actor`,
    COUNT(FA.film_id) AS `# of Movies`
FROM
    actor AS A
        JOIN
    film_actor AS FA ON A.actor_id = FA.actor_id
GROUP BY A.actor_id;