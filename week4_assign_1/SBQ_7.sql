USE sakila;
/* A VIEW for needed value*/
CREATE VIEW temp AS SELECT first_name
FROM
  actor
WHERE
  actor_id = 8;
/* Similar first name but not the same id*/  
SELECT 
    A.first_name, A.last_name
FROM
    actor AS A,
    temp AS T
WHERE
    A.first_name = T.first_name
        AND A.actor_id != 8 
UNION SELECT 
    C.first_name, C.last_name
FROM
    customer AS C,
    temp AS T
WHERE
    C.first_name = T.first_name