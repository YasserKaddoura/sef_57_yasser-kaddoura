USE sakila;

SELECT 
    L.name AS `Language`, COUNT(F.language_id) AS `#`
FROM
    film AS F
        JOIN
    language AS L ON F.language_id = L.language_id
        AND F.release_year = 2006
GROUP BY F.language_id
ORDER BY `#` DESC
LIMIT 3;