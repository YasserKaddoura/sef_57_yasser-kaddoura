USE sakila;

SELECT 
    CONCAT(C.first_name, ' ', C.last_name) AS Customer,
    COUNT(C.customer_id) AS `# of rentals`
FROM
    customer AS C
        JOIN
    rental AS R ON C.customer_id = R.customer_id
        AND YEAR(R.rental_date) = 2005
GROUP BY C.customer_id
ORDER BY `# of rentals` DESC
LIMIT 3;