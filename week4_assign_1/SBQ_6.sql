USE sakila;

SELECT 
    T.name AS name, T.total AS `#`
FROM /* Get the top 3 not in range + all in range*/
    ((SELECT 
        C.name AS name, COUNT(C.name) AS total
    FROM
        film_category AS FC
    JOIN category AS C ON FC.category_id = C.category_id
    GROUP BY C.category_id
    HAVING COUNT(C.name) < 55 OR COUNT(C.name) > 65
    LIMIT 3) UNION SELECT 
        C.name AS name, COUNT(C.name) AS total
    FROM
        film_category AS FC
    JOIN category AS C ON FC.category_id = C.category_id
    GROUP BY C.category_id
    HAVING COUNT(C.name) BETWEEN 55 AND 65) AS T
WHERE /* Get tuples depending on the condition*/
    IF((SELECT 
                COUNT(*)
            FROM
                (SELECT 
                    C.name AS name, COUNT(C.name)
                FROM
                    film_category AS FC
                JOIN category AS C ON FC.category_id = C.category_id
                GROUP BY C.category_id
                HAVING COUNT(C.name) BETWEEN 55 AND 65) AS temp) > 0,
        total BETWEEN 55 AND 65,
        total < 55 OR total > 65)
ORDER BY `#` DESC;