USE sakila;

SELECT 
    ST.store_id AS `Store`,
    SUM(P.amount) AS `SUM`,
    AVG(P.amount) AS `AVERAGE`,
    MONTH(P.payment_date) AS `MONTH`,
    YEAR(P.payment_date) AS `YEAR`
FROM
    payment AS P
        JOIN
    staff AS ST ON ST.staff_id = P.staff_id
GROUP BY ST.store_id , MONTH(P.payment_date) , YEAR(P.payment_date);
