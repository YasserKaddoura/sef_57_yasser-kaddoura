USE sakila;

SELECT 
    CONCAT(A.first_name, ' ', A.last_name) AS `Actor`,
    F.release_year AS `Release Year`
FROM
    actor AS A
        JOIN
    film_actor AS FM ON A.actor_id = FM.actor_id
        JOIN
    film AS F ON FM.film_id = F.film_id
WHERE
    F.description LIKE '%Crocodile%'
        AND F.description LIKE '%Shark%'
ORDER BY A.last_name;