USE sakila;

SELECT 
    C.country, COUNT(C.country) AS `#`
FROM
    country AS C
        JOIN
    city AS CT ON CT.country_id = C.country_id
        JOIN
    address AS A ON A.city_id = CT.city_id
        JOIN
    customer AS CST ON CST.address_id = A.address_id
GROUP BY C.country
ORDER BY `#` DESC
LIMIT 3;
