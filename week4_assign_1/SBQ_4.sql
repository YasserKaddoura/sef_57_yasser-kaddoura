USE sakila;

SELECT 
    address2
FROM
    address AS A
WHERE
    A.address2 != ''
ORDER BY A.address2;